definition {
    name = "HamiltonianCycle"
    title = "Гамильтонов цикл"
    description = "Нарисовать (раскрасить ребра) гамильтонов цикл - цикл, проходящий через каждую вершину графа один раз."

    import {
        builtin
    }

    python {
        def draw_hamiltonian_cycle(graph, start, vertex, color, explored=None, length=0):
            if explored is None:
                explored = []
            explored.append(vertex)
            for edge in vertex.edges:
                if edge.color != color:
                    v = edge.opposite(vertex)
                    if v is start:
                        if length >= len(graph) - 1:
                            edge.color = color
                            return True
                    if v not in explored:
                        _color = edge.color
                        edge.color = color
                        if draw_hamiltonian_cycle(graph, start, v, color, explored=explored, length=length + 1):
                            return True
                        edge.color = _color
            explored.remove(vertex)
            return False

        def is_hamiltonian_cycle(graph, color):
            def check_vertex(v):
                for edge in v.edges:
                    if edge.color == color:
                        return True
                return False
            cycle = graph.from_condition(check_vertex, lambda e: e.color == color)
            for vertex in cycle:
                if len(vertex.edges) != 2:
                    return False
            return len(cycle) == len(graph)

        def apply(graph, **kw):
            for color in graph.get_edge_colors():
                if is_hamiltonian_cycle(graph, color):
                    return True
            return False

        def generate_solution(graph, **kw):
            graph = graph.copy()
            color = 1
            colors = graph.get_edge_colors()
            while color in colors:
                color += 1

            if draw_hamiltonian_cycle(graph, graph[0], graph[0], color):
                return graph

        def generate_example(**kw):
            graph = Graph()
            for i in range(random.randint(7, 11)):
                graph.add_vertex(connections=[i - 1] if i > 0 else [])
            graph.add_vertex(connections=[0, -2])
            for edge in graph.get_edges():
                edge.color = 1
            graph.randomize(iterations=int(len(graph) * 0.67), group_chance=0, vertex_chance=0)
            return graph
            while True:
                graph = basic_generate(vertices=(6, 10), edges_per_vertex=(2, 2), condition=lambda graph: graph.connected())
                if draw_hamiltonian_cycle(graph, graph[0], graph[0], 1):
                    return graph
    }

    apply = apply
    generate_example = generate_example
    generate_solution = generate_solution

    flags = ACTION_PAINT
}