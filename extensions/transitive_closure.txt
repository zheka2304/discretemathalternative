definition {
    name = "TransitiveClosure"
    title = "Транзитивное Замыкание"
    description = "Построить минимальное транзитивное замыкание данного графа. Транзитивным замыканием называется дополнение графа на котором выполняется транзитивность: если A->B и B->C то существует A->C."

    import {
        builtin
    }

    python {
        def is_transitive_closure(graph):
            for edge in graph.get_edges():
                if edge.t_transitive:
                    changed = False
                    for edge_ in edge.end.edges:
                        if edge_.t_transitive and edge_.start is edge.end:
                            connection = edge.start.edge(edge_.end)
                            if connection is None or not connection.directed or connection.start is not edge.start:
                                return False
            return True

        # mark t_transitive all edges and vertices of transitive closure of sub-graph marked as t_transitive
        def transitive_closure(graph):
            while True:
                for edge in graph.get_edges():
                    if edge.t_transitive:
                        changed = False
                        for edge_ in edge.end.edges:
                            if edge_.t_transitive and edge_.start is edge.end:
                                connection = edge.start.edge(edge_.end)
                                if connection is None:
                                    connection = edge.start.connect(edge_.end)
                                    connection.t_transitive = True
                                    changed = True
                                else:
                                    if not connection.t_transitive:
                                        changed = True
                                    connection.t_transitive = True
                                if connection.start is not edge.start:
                                    connection.redirect()
                                    changed = True
                                connection.directed = True
                        if changed:
                            break
                else:
                    break

        def apply(graph, original=None, **kw):
            if original is not None:
                user_graph = graph.from_active()
                transitive_graph = user_graph.copy()

                def activate_transitive(edge):
                    edge.t_transitive = edge.active
                user_graph.for_edges(activate_transitive)
                if not is_transitive_closure(user_graph):
                    return False

                def activate_transitive(edge):
                    edge.t_transitive = edge.locked
                transitive_graph.for_edges(activate_transitive)
                transitive_closure(transitive_graph)

                transitive_edges = 0
                for edge in transitive_graph.get_edges():
                    if edge.t_transitive:
                        transitive_edges += 1
                return transitive_edges == len(user_graph.get_edges())


        def generate_example():
            iterations = 0
            while True:
                graph = basic_generate(vertices=(5, 8), edges_per_vertex=(12, 12))
                def randomize_direction(edge):
                    edge.directed = True
                    if random.randint(0, 1) == 0:
                        edge.redirect()
                graph.for_edges(randomize_direction)
                edges = list(graph.get_edges())
                edges = edges[:random.randint(len(edges) // 4, len(edges) // 3)]
                for edge in edges:
                    edge.locked = edge.start.locked = edge.end.locked = True
                    edge.color = 1

                def activate_transitive(edge):
                    edge.t_transitive = edge.locked
                graph.for_edges(activate_transitive)
                transitive_closure(graph)

                count = 0
                for edge in graph.get_edges():
                    edge.active = edge.t_transitive
                    if not edge.locked and edge.t_transitive:
                        count += 1

                for vertex in graph:
                    for edge in vertex.edges:
                        if edge.t_transitive:
                            break
                    else:
                        vertex.active = False
                        vertex.locked = False

                iterations += 1
                if 1 <= len(edges) // 2 <= count <= len(edges):
                    return graph


    }

    apply = apply
    generate_example = generate_example
    flags = ACTION_EDIT, ACTION_DIRECT, FLAG_NO_CROSS
}