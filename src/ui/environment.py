import os
import uuid

from time import sleep, perf_counter_ns
from task import *


class MessageHandle:
    def __init__(self):
        self.callbacks = []
        self.suspended_messages = []
        self.suspended = False

    def add_callback(self, callback):
        self.callbacks.append(callback)

    def suspend(self, resume=False):
        self.suspended = resume

    def resume(self):
        self.suspend(resume=True)
        self.report_suspended()

    def pop_and_report(self):
        if len(self.suspended_messages) > 0:
            self.instant_report(**self.suspended_messages.pop(0))

    def report_suspended(self):
        while len(self.suspended_messages) > 0:
            self.pop_and_report()

    def instant_report(self, message=None, exception=None, source=None, **kwargs):
        if exception is not None:
            def _raise():
                raise exception
            Thread(target=_raise).start()
        for callback in self.callbacks:
            callback(message=message, exception=exception, source=source, **kwargs)

    def report(self, **kwargs):
        if self.suspended:
            self.suspended_messages.append(kwargs)
        else:
            self.instant_report(**kwargs)


class JobThreadScheduler:
    # lock_time - time between yields in ms
    # yield_time - yield sleep time in ms
    def __init__(self, lock_time=150, yield_time=1):
        self.lock_time = lock_time
        self.yield_time = yield_time * 1e-3
        self.last_yield = -1

    def yield_(self):
        if self.last_yield == -1 or perf_counter_ns() * 1e-6 > self.last_yield:
            sleep(self.yield_time)
            self.last_yield = perf_counter_ns() * 1e-6 + self.lock_time


class JobInstance:
    def __init__(self, work, title="Work in progress...", family=None, lock_time=150, yield_time=1,
                 start=lambda **kw: None, end=lambda **kw: None):
        self.uuid = str(uuid.uuid4())
        self.title = title
        self.family = family
        self.scheduler = JobThreadScheduler(lock_time=lock_time, yield_time=yield_time)

        self._work = work
        self._start = start
        self._end = end
        self._progress = 0
        self._complete = False
        self._terminated = False
        self._error = None

    def error(self):
        return self._error

    def terminate(self):
        self._terminated = True

    def terminated(self):
        return self._terminated

    def complete(self):
        return self._complete

    def progress(self, *args):
        if args:
            self._progress = args[0]
        else:
            return self._progress

    def work(self, message_handle):
        try:
            self._start(job=self)
            self._work(job=self)
            self._end(job=self)
        except Exception as err:
            self._error = err
            if message_handle is not None:
                message_handle.report(message="exception in job " + self.title, exception=err)
        self._complete = True


class JobManager:
    def __init__(self, message_handle):
        self.jobs = []
        self.job_families = {}
        self.message_handle = message_handle

        self._lock = Lock()
        self._update_callbacks = []

    def add_job_update_callback(self, callback):
        self._update_callbacks.append(callback)

    def _on_jobs_update(self):
        for callback in self._update_callbacks:
            callback(self.jobs[:])

    def add(self, *args, **kwargs):
        job = JobInstance(*args, **kwargs)
        family = job.family

        def run():
            self.jobs.append(job)
            self._on_jobs_update()
            job.work(self.message_handle)
            self.jobs.remove(job)
            self._on_jobs_update()
            with self._lock:
                if family is not None and family in self.job_families and run in self.job_families[family]:
                    self.job_families[family].remove(run)

        def run_family():
            while True:
                with self._lock:
                    if family in self.job_families and len(self.job_families[family]) > 0:
                        run_job = self.job_families[family].pop(0)
                    else:
                        break
                run_job()
            with self._lock:
                del self.job_families[family]

        with self._lock:
            if family is not None:
                if family not in self.job_families:
                    self.job_families[family] = [run]
                    Thread(target=run_family).start()
                else:
                    self.job_families[family].append(run)
            else:
                Thread(target=run).start()


class EventQueue:
    def __init__(self):
        self.queue = []
        self.callbacks = {}

    def add_callback(self, event, callback):
        if event not in self.callbacks:
            self.callbacks[event] = []
        self.callbacks[event].append(callback)

    def push(self, event, **kwargs):
        self.queue.append((event, kwargs))

    def _has_event(self, name):
        for event, kwargs in self.queue:
            if event == name:
                return True
        return False

    def _pop_event(self, name):
        for event, kwargs in self.queue:
            if event == name:
                self.queue.remove((event, kwargs))
                return event, kwargs

    def invoke_all(self, wrap=lambda f, **kwargs: f(**kwargs)):
        queue = self.queue
        self.queue = []
        for name, kwargs in queue:
            if name in self.callbacks:
                for callback in self.callbacks[name]:
                    wrap(callback, **kwargs)

    def pop(self, *args, everything=False):
        if args:
            events = set()
            for event in args:
                while self._has_event(event):
                    self._pop_event(event)
                    events.add(event)
                    if not everything:
                        return events
            if everything:
                return list(events)
            else:
                return None
        else:
            if everything:
                events = [x[0] for x in self.queue]
                self.queue = []
                return events
            else:
                if len(self.queue) > 0:
                    return self.queue.pop(0)[0]
                else:
                    return None


class TaskEnvironment:
    def __init__(self, visualizer):
        self.events = EventQueue()
        self.message_handle = MessageHandle()
        self.job_manager = JobManager(self.message_handle)

        self.visualizer = visualizer
        self.extensions = ExtensionManager()
        self.task_manager = TaskManager(self.visualizer, self.extensions, self.message_handle)

        self.job_manager.add_job_update_callback(lambda jobs: self.events.push("jobs_update", jobs=jobs))

        self.sources = []
        if self.visualizer is not None:
            self.visualizer.job_manager = self.job_manager

    def add_source(self, source):
        self.sources.append(source)

    def reload_extensions(self):
        self.extensions.clear()

        for source in self.sources:
            if source[-1] in ("/", "\\"):  # directory
                files = []
                try:
                    for (dir_path, dir_names, file_names) in os.walk(source):
                        files.extend([os.path.join(dir_path, name) for name in file_names])
                except Exception as err:
                    self.message_handle.report(exception=err)
            else:
                files = []

            for file in files:
                self.extensions.load(file=file, silent=True)

        self.extensions.initialize(silent=True)
        for ext in self.extensions.extensions:
            if not ext.valid():
                self.message_handle.report(
                    message="error in loading extension " + ext.filename(),
                    exception=ext.error()
                )

        self.events.push("reload_task_list")
        self.events.push("reload_definition_list")
        self.events.push("reload_extension_tree")

    def get_task(self, uid):
        return self.extensions.tasks[uid]

    def get_definition(self, uid):
        return self.extensions.definitions[uid]

    def get_current_solution(self):
        return self.task_manager.get_current()

    # return list of tasks and names
    def list_tasks(self):
        tasks = {}
        for uid in self.extensions.get_task_uuids():
            tasks[uid] = self.get_task(uid)
        return tasks

    def list_definitions(self):
        definitions = {}
        for uid in self.extensions.get_definition_uuids():
            definitions[uid] = self.extensions.definitions[uid]
        return definitions

    def draw_graph(self, graph):
        uid = str(uuid.uuid4())
        self.visualizer.draw(graph, None, uid)
        return uid

    def erase_graph(self, uid):
        self.visualizer.erase(uid)

    def load_task(self, uid):
        try:
            self.task_manager.load(self.extensions.tasks[uid])
            self.events.push("task_loaded")
        except Exception as err:
            self.message_handle.report(message="error while loading task " + uid, exception=err)

    def load_anonymous_task(self, task):
        try:
            self.task_manager.load(task)
            self.events.push("task_loaded")
        except Exception as err:
            self.message_handle.report(message="error while loading task " + task.uuid, exception=err)

    def unload_task(self):
        self.task_manager.unload()
        self.events.push("task_unloaded")

    def generate_cross_definition(self, **kwargs):
        definition = self.extensions.generate_random_cross_definition(**kwargs)
        if definition is not None:
            task = self.extensions.task_from_definition(definition)
            if task is not None:
                task.combined = True
            self.events.push("reload_task_list")
            self.events.push("reload_definition_list")
        return definition

    def generate_procedural_tasks(self):
        self.extensions.generate_procedural_tasks()
        self.events.push("reload_task_list")
        self.events.push("reload_definition_list")

    def start_new_exercise(self, definitions=None, random_count=3, **kwargs):
        if definitions is None:
            valid_definitions = [d for d in self.extensions.definitions.values() if d.validated]
            random.shuffle(valid_definitions)
            definitions = valid_definitions[:min(len(valid_definitions), random_count)]
        if len(definitions) > 0:
            exercise = Exercise(definitions)
            exercise.build(**kwargs)
            if len(exercise.tasks) > 0:
                return exercise
