from tkinter import *
from tkinter import messagebox
from tkinter.ttk import Treeview, Notebook

from random import *
from threading import *
from time import sleep

from task import *
from visualization import *
from .environment import *
from .widgets import *


class Window(Tk):
    def __init__(self, *args, title="Discrete Math", resolution=(800, 500), padding=25, surroundings=(200, 0), **kwargs):
        Tk.__init__(self, *args, **kwargs)

        self.window_title = title
        self.resolution = resolution
        self.surroundings = surroundings
        self.padding = padding

        r_width, r_height = resolution
        self.sqr_screen = (0, (r_height - r_width) / 2, r_width, r_width + (r_height - r_width) / 2) \
                            if r_width < r_height else \
                            ((r_width - r_height) / 2, 0, r_height + (r_width - r_height) / 2, r_height)

        self.container = InterfaceGroupContainer()
        self.visualizer = TaskVisualizer(self.container, screen=(
            self.sqr_screen[0] + padding, self.sqr_screen[1] + padding,
            self.sqr_screen[2] - padding, self.sqr_screen[3] - padding,
        ))
        self.environment = TaskEnvironment(self.visualizer)

        self._init_window()
        self._init_canvas()
        self._init_context_menu()
        self._init_side_widgets()
        self._init_bottom_widgets()
        self._init_events()

        self.container.canvas = self.canvas
        self._running = True
        self._render_thread = Thread(target=self._render_loop)

    def _init_window(self):
        width = self.resolution[0] + self.surroundings[0]
        height = self.resolution[1] + self.surroundings[1]
        self.title(self.window_title)
        self.geometry("{}x{}+200+0".format(width, height))
        self.minsize(width, height)

        self._main_frame = Frame()
        self._main_frame.grid(row=0, column=0, sticky="nsew")
        self._side_frame = Frame()
        self._side_frame.grid(row=0, column=1, sticky="nsew")
        self.rowconfigure(0, weight=1)
        self.columnconfigure(1, weight=1)

    def _init_canvas(self):
        width, height = self.resolution

        frame = Frame(self._main_frame, width=width, height=height)
        frame.grid(row=0, column=0, sticky="nsew")
        self._side_frame.columnconfigure(0, weight=1)

        self.canvas = Canvas(frame, bg="white", width=width, height=height)
        self.canvas.grid(row=0, column=0, sticky="nsew")

        self.canvas.bind("<ButtonRelease-1>", lambda event: self.action("left", event))
        self.canvas.bind("<ButtonRelease-3>", lambda event: self.action("right", event))
        self.canvas.bind("<Double-Button-1>", lambda event: self.action("double", event))
        self.canvas.bind("<B1-Motion>", lambda event: self.action("drag", event))
        self.canvas.bind("<Motion>", lambda event: self.action("highlight", event))

        save_graph_button = Button(self.canvas, text="Save Graph", bg="white",
                                   command=self._save_current_displayed_graph)
        self.canvas.create_window(10, 10, window=save_graph_button, anchor="nw")

    def _init_context_menu(self):
        self._create_context_menu(aa=None)

    def _create_context_menu(self, callback=False, parent=None, **commands):
        if parent is None:
            parent = self
        menu = Menu(parent, tearoff=0)
        for label, command in commands.items():
            menu.add_command(label=label, command=command)
        if callback:
            def popup(event):
                try:
                    menu.tk_popup(event.x_root + 20, event.y_root + 20, 0)
                finally:
                    menu.grab_release()
            return popup
        else:
            return menu

    def _init_side_widgets(self):
        frame = Frame(self._side_frame, padx=3, pady=3)
        frame.grid(row=0, column=0, sticky="nsew")
        frame.columnconfigure(0, weight=1)  # row for all controls
        frame.rowconfigure(1, weight=1)     # row for task
        self._side_frame.rowconfigure(0, weight=1)
        self._side_frame.columnconfigure(0, weight=1)

        # task frame
        self._current_selected_task = None
        self._task_frame = Frame(frame)
        self._task_frame.columnconfigure(0, weight=1)
        self._task_frame.rowconfigure(2, weight=1)
        self._task_frame_title = Message(self._task_frame, text="<Title>", justify=CENTER,
                                         font=("", 18), bg="#ddf", fg="#333")
        self._task_frame_title.bind("<Configure>",
                                    lambda event: self._task_frame_title.configure(width=event.width - 10))
        self._task_frame_title.grid(row=0, column=0, sticky="nsew")
        self._task_frame_description = Message(self._task_frame,
                                               bg="#ddf", fg="#444", anchor="w",
                                               text="<Description>")
        self._task_frame_description.bind("<Configure>",
                                          lambda event: self._task_frame_description.configure(width=event.width - 10))
        self._task_frame_description.grid(row=2, column=0, sticky="new")

        task_controls_frame = Frame(self._task_frame, relief=GROOVE, borderwidth=1)
        task_controls_frame.grid(row=4, column=0, sticky="nsew")
        task_controls_frame.columnconfigure(0, weight=1)
        Button(task_controls_frame, text="Close", command=self._exit_from_task, bg="#fbb").grid(row=1, column=0, sticky="nsew", padx=3, pady=3)
        self._task_controls_bottom = Frame(task_controls_frame)
        self._task_controls_bottom.grid(row=1, column=0, sticky="nsew")
        self._task_controls_bottom.columnconfigure(0, weight=1)
        self._task_controls_bottom.columnconfigure(1, weight=1)
        self._task_controls_bottom.columnconfigure(2, weight=1)
        Button(self._task_controls_bottom, text="Start", command=self._load_selected_task, bg="#bbf").grid(row=0, column=0, sticky="nsew", padx=3, pady=3)
        Button(self._task_controls_bottom, text="Check", command=self._check_task, bg="#dfb").grid(row=0, column=1, sticky="nsew", padx=3, pady=3)
        Button(self._task_controls_bottom, text="Close", command=self._exit_from_task, bg="#fbb").grid(row=0, column=2, sticky="nsew", padx=3, pady=3)
        self._task_frame_forget_button = Button(task_controls_frame, text="Forget Solution", bg="#f99", command=self._forget_current_solution)

        # definition frame
        self._current_selected_definition = None
        self._current_definition_example = None
        self._current_definition_example_uid = None
        self._definition_frame = Frame(frame)
        self._definition_frame.columnconfigure(0, weight=1)
        self._definition_frame.rowconfigure(2, weight=1)
        self._definition_frame_title = Message(self._definition_frame, text="<Title>", justify=CENTER, font=("", 18), bg="#ddf", fg="#333")
        self._definition_frame_title.bind("<Configure>", lambda event: self._definition_frame_title.configure(width=event.width - 10))
        self._definition_frame_title.grid(row=0, column=0, sticky="nsew")
        self._definition_frame_description = Message(self._definition_frame, bg="#ddf", fg="#444", anchor="w", text="<Description>")
        self._definition_frame_description.bind("<Configure>", lambda event: self._definition_frame_description.configure(width=event.width - 10))
        self._definition_frame_description.grid(row=2, column=0, sticky="new")

        definition_controls_frame = Frame(self._definition_frame, relief=GROOVE, borderwidth=1)
        definition_controls_frame.grid(row=4, column=0, sticky="nsew")
        definition_controls_frame.columnconfigure(0, weight=1)
        self._definition_controls_bottom = Frame(definition_controls_frame)
        self._definition_controls_bottom.grid(row=1, column=0, sticky="nsew")
        self._definition_controls_bottom.columnconfigure(0, weight=1)
        Button(self._definition_controls_bottom, text="Example", command=self._generate_definition_example, bg="#dfb").grid(row=0, column=0, sticky="nsew", padx=3, pady=3)
        Button(self._definition_controls_bottom, text="Close", command=self._exit_from_definition, bg="#fbb").grid(row=1, column=0, sticky="nsew", padx=3, pady=3)

        # control panel
        self._control_frame = Frame(frame)
        self._control_frame.grid(row=1, column=0, sticky="nsew")
        self._control_frame.columnconfigure(0, weight=1)

        control_panel_frame = Frame(self._control_frame, padx=5, pady=5, relief=GROOVE, borderwidth=2)
        control_panel_frame.grid(row=2, column=0, sticky="nsew")
        control_panel_frame.columnconfigure(0, weight=1)

        Label(control_panel_frame, text="Control Panel").grid(row=0, column=0, sticky="nsew")
        Button(control_panel_frame, text="Reload Extensions",
               command=self._start_extension_reload).grid(row=1, column=0, sticky="nsew")
        Button(control_panel_frame, text="Remove All Generated",
               command=self._remove_all_procedural).grid(row=2, column=0, sticky="nsew")
        Button(control_panel_frame, text="Forget All Solutions",
               command=self._forget_all_solutions).grid(row=3, column=0, sticky="nsew")

        self._jobs_label_frame = Frame(control_panel_frame)
        self._jobs_label_frame.grid(row=4, column=0, sticky="nsew")
        self._jobs_label_frame.columnconfigure(0, weight=1)
        self._jobs_label_frame.columnconfigure(3, weight=1)
        self._jobs_label = Label(self._jobs_label_frame, text="No background tasks", fg="#aaa")
        self._jobs_label.grid(row=0, column=2, sticky="nsew", pady=3)
        self._jobs_label_spinner = LoadingSpinner(self._jobs_label_frame, size=(20, 20))

        # task list
        Label(self._control_frame, text="Available:").grid(row=3, column=0, sticky="nsew")

        notebook = Notebook(self._control_frame)
        notebook.grid(row=4, column=0, sticky="nsew")

        task_list_frame = Frame(self._control_frame, bg="white")
        task_list_frame.columnconfigure(0, weight=1)
        task_list_frame.rowconfigure(0, weight=1)
        notebook.add(task_list_frame, text="All Tasks")

        self._task_listbox_uuids = []
        self._task_listbox = Listbox(task_list_frame)
        self._task_listbox.grid(row=0, column=0, sticky="nsew")
        task_list_scroll = Scrollbar(task_list_frame, orient=VERTICAL)
        task_list_scroll.grid(row=0, column=1, sticky="nsew")
        task_list_scroll.config(command=self._task_listbox.yview)
        self._task_listbox.configure(yscrollcommand=task_list_scroll.set)

        def _select_task(*args):
            selection = self._task_listbox.curselection()
            if len(selection) > 0:
                self._select_task(self.environment.get_task(self._task_listbox_uuids[selection[0]]))
        self._task_listbox.bind("<Double-1>", _select_task)

        def _generate_new_condition(*args):
            selection = self._task_listbox.curselection()
            if len(selection) > 0:
                task = self.environment.get_task(self._task_listbox_uuids[selection[0]])
                if task is not None:
                    task.randomize_parameters()
                else:
                    print("internal error: selected task cannot be found")
            else:
                messagebox.showerror("Error", "Select a task in list first!")
        self._task_listbox.bind("<Button-3>", self._create_context_menu(callback=True, **{
            "Generate New Condition": _generate_new_condition
        }))

        definition_frame = Frame(self._control_frame)
        definition_frame.columnconfigure(0, weight=1)
        definition_frame.rowconfigure(1, weight=1)
        notebook.add(definition_frame, text="Definitions")
        Button(definition_frame, text="Generate Definitions", bg="#abf",
               command=self._start_cross_definition_generation).grid(row=0, column=0, sticky="nsew", padx=3, pady=3)

        definition_list_frame = Frame(definition_frame, bg="white")
        definition_list_frame.grid(row=1, column=0, sticky="nsew")
        definition_list_frame.columnconfigure(0, weight=1)
        definition_list_frame.rowconfigure(1, weight=1)

        self._definition_listbox_uuids = []
        self._definition_listbox = Listbox(definition_list_frame)
        self._definition_listbox.grid(row=0, column=0, sticky="nsew")
        definition_list_scroll = Scrollbar(definition_list_frame, orient=VERTICAL)
        definition_list_scroll.grid(row=0, column=1, sticky="nsew")
        definition_list_scroll.config(command=self._definition_listbox.yview)
        self._definition_listbox.configure(yscrollcommand=definition_list_scroll.set)

        def _select_definition(*args):
            selection = self._definition_listbox.curselection()
            if len(selection) > 0:
                self._select_definition(self.environment.get_definition(self._definition_listbox_uuids[selection[0]]))
        self._definition_listbox.bind("<Double-1>", _select_definition)

        exercise_frame = Frame(self._control_frame)
        exercise_frame.columnconfigure(0, weight=1)
        exercise_frame.rowconfigure(1, weight=1)
        notebook.add(exercise_frame, text="Exercise")
        Button(exercise_frame, text="Start New Exercise", bg="#bf8",
               command=self._start_new_exercise).grid(row=0, column=0, sticky="nsew", padx=3, pady=3)

        exercise_list_frame = Frame(exercise_frame, bg="white")
        exercise_list_frame.grid(row=1, column=0, sticky="nsew")
        exercise_list_frame.columnconfigure(0, weight=1)
        exercise_list_frame.rowconfigure(0, weight=1)

        self._current_exercise = None
        self._exercise_listbox_tasks = []
        self._exercise_listbox = Listbox(exercise_list_frame)
        self._exercise_listbox.grid(row=0, column=0, sticky="nsew")
        exercise_list_scroll = Scrollbar(exercise_list_frame, orient=VERTICAL)
        exercise_list_scroll.grid(row=0, column=1, sticky="nsew")
        exercise_list_scroll.config(command=self._exercise_listbox.yview)
        self._exercise_listbox.configure(yscrollcommand=exercise_list_scroll.set)

        def _select_exercise_task(*args):
            selection = self._exercise_listbox.curselection()
            if len(selection) > 0:
                self._select_task(self._exercise_listbox_tasks[selection[0]])
        self._exercise_listbox.bind("<Double-1>", _select_exercise_task)

        # extension tree
        Label(self._control_frame, text="Extensions:").grid(row=5, column=0, sticky="nsew")

        extension_tree_frame = Frame(self._control_frame, bg="white")
        extension_tree_frame.grid(row=6, column=0, sticky="nsew")
        extension_tree_frame.columnconfigure(0, weight=1)
        extension_tree_frame.rowconfigure(0, weight=1)

        self._extension_tree = Treeview(extension_tree_frame,  show="tree")
        self._extension_tree.grid(row=0, column=0, sticky="nsew")
        extension_tree_scroll = Scrollbar(extension_tree_frame, orient=VERTICAL)
        extension_tree_scroll.grid(row=0, column=1, sticky="nsew")
        extension_tree_scroll.config(command=self._extension_tree.yview)
        self._extension_tree.configure(yscrollcommand=extension_tree_scroll.set)
        self._extension_tree.bind("<Button-3>", self._create_context_menu(callback=True, **{
            "Expand All": lambda *args: self._extensions_set_all_expanded(True),
            "Collapse All": lambda *args: self._extensions_set_all_expanded(False)
        }))

    def _init_bottom_widgets(self):
        frame = Frame(self._main_frame, bg="#faa")
        frame.grid(row=1, column=0, sticky="nsew")
        frame.columnconfigure(0, weight=1)
        frame.rowconfigure(0, weight=1)

        self._main_frame.rowconfigure(1, weight=1)

        self._console = Console(frame)
        self._console.grid(row=0, column=0, sticky="nsew")
        self._console.bind("<Button-3>", self._create_context_menu(callback=True, **{
            "Clear Console": lambda *args: self._console.clear()
        }))

        def console_callback(message=None, exception=None, **kwargs):
            if message:
                self._console.write(str(message), error=bool(exception))
            if exception:
                prefix = ""
                if message:
                    prefix = "    "
                self._console.error(prefix + str(exception))
        self.environment.message_handle.add_callback(console_callback)

    #

    def _start_extension_reload(self):
        self.environment.job_manager.add(lambda **kwargs: self.environment.reload_extensions(),
                                         family="extension_reload",
                                         title="Reloading Extensions...")
        self.environment.job_manager.add(lambda **kwargs: self.environment.generate_procedural_tasks(),
                                         family="extension_reload",
                                         title="Generating Procedural Tasks...")

    def _start_cross_definition_generation(self):
        def generate_definitions(job=None, **kwargs):
            while self.environment.generate_cross_definition(scheduler=job.scheduler) is not None:
                pass
        self.environment.job_manager.add(generate_definitions, lock_time=150,
                                         family="gen_cross_definitions",
                                         title="Generating Cross Definitions...")

    def _start_task_generation(self):
        self.environment.job_manager.add(lambda **kwargs: self.environment.generate_procedural_tasks(),
                                         family="gen_procedural_tasks",
                                         title="Generating New Tasks...")

    def _start_new_exercise(self):
        def generate(**kwargs):
            dialog = ExerciseDialog()
            dialog.set_definitions({d.uuid: self._to_human_name(d.title())
                                    for d in self.environment.extensions.definitions.values() if d.validated})
            result = dialog.get_result()
            if result is not None:
                definitions, total_tasks, random_count = result
                if definitions is not None:
                    total_tasks = len(definitions) * random_count
                    definitions = [self.environment.extensions.definitions[uid] for uid in definitions]
                exercise = self.environment.start_new_exercise(definitions=definitions,
                                                               total_tasks=total_tasks,
                                                               random_count=random_count)
                if exercise is not None:
                    self._current_exercise = exercise
                    self.environment.events.push("reload_task_list")
                else:
                    messagebox.showerror("", "Failed to generate exercise for some reason.")
        self.environment.job_manager.add(generate,
                                         family="gen_exercise",
                                         title="Creating Exercise...")

    def _check_exercise_solved(self):
        if self._current_exercise is not None and self._current_exercise.is_solved():
            if self._confirm_action("Exercise is complete, generate a new one?"):
                self._current_exercise = None
                self._start_new_exercise()

    def _save_current_displayed_graph(self):
        def save_graph(**kwargs):
            current_graph = None
            if self._current_selected_task is not None:
                current_graph = self.environment.get_current_solution()
                if current_graph is None and self._current_selected_task.is_solved():
                    current_graph = self._current_selected_task.get_solutions()[-1]
            if current_graph is None and self._current_definition_example:
                current_graph = self._current_definition_example
            if current_graph is not None and len(current_graph) > 0:
                serialized = current_graph.serialize()
                save_name = SaveGraphDialog(data=serialized).get_result()
                if save_name is not None:
                    graph_saver.save_graph(save_name, current_graph)
        self.environment.job_manager.add(save_graph, family="save_graph", title="Saving Graph...")

    def _remove_all_procedural(self):
        if self._confirm_action("Remove all procedurally generated tasks and definitions?"):
            self.environment.extensions.clear_procedural_tasks()
            self.environment.extensions.clear_procedural_definitions()
            self.environment.generate_procedural_tasks()

    def _forget_all_solutions(self):
        if self._confirm_action("Forget all your solutions?"):
            for uid, task in self.environment.list_tasks().items():
                task.forget_solutions()
            self.environment.events.push("reload_task_list")

    def _extensions_set_all_expanded(self, expanded, root=None):
        for child in self._extension_tree.get_children(root):
            self._extension_tree.item(child, open=expanded)
            self._extensions_set_all_expanded(expanded, root=child)

    def _confirm_action(self, message, title=None, default=True):
        return messagebox.askokcancel("" if title is None else title,
                                      "" if message is None else message,
                                      default="ok" if default else "cancel")

    #

    def _open_task_frame(self):
        self._definition_frame.grid_remove()
        self._control_frame.grid_remove()
        self._task_frame.grid(row=1, column=0, sticky="nsew")

    def _open_definition_frame(self):
        self._control_frame.grid_remove()
        self._task_frame.grid_remove()
        self._definition_frame.grid(row=1, column=0, sticky="nsew")

    def _open_control_frame(self):
        self._definition_frame.grid_remove()
        self._task_frame.grid_remove()
        self._control_frame.grid(row=0, column=0, sticky="nsew")

    def _update_task_buttons(self):
        if self._current_selected_task is not None and self._current_selected_task.is_solved():
            self._task_frame_forget_button.grid(row=0, column=0, sticky="nsew", padx=3, pady=(3, 0))
            self._task_controls_bottom.grid_forget()
        else:
            self._task_frame_forget_button.grid_forget()
            self._task_controls_bottom.grid(row=1, column=0, sticky="nsew")

    def _get_current_task(self):
        return self.environment.task_manager.get_current_task()

    def _forget_current_solution(self):
        if self._current_selected_task is not None and self._current_selected_task.is_solved():
            self._current_selected_task.forget_solutions()
            self._update_task_buttons()
            self.environment.task_manager.unload()
            self.environment.events.push("reload_task_list")

    def _select_task(self, task):
        if self._get_current_task() is None or self._confirm_action("select new task?"):
            self._open_task_frame()
            self._current_selected_task = task
            self._task_frame_title.configure(text=self._to_human_name(self._current_selected_task.name()))
            self._task_frame_description.configure(text=self._current_selected_task.description())
            self._update_task_buttons()
            if self._current_selected_task is not None and self._current_selected_task.is_solved():
                solution = self._current_selected_task.get_solutions()[-1]
                self.environment.task_manager.show_solution(solution)

    def _load_selected_task(self):
        if self._current_selected_task is not None:
            if self._get_current_task() is None or \
                self._get_current_task() == self._current_selected_task and self._confirm_action("restart task?") or \
                    self._get_current_task() != self._current_selected_task and \
                    self._confirm_action("exit current and start another task?"):
                if self._current_selected_task.anonymous:
                    self.environment.load_anonymous_task(self._current_selected_task)
                else:
                    self.environment.load_task(self._current_selected_task.uuid)

    def _exit_from_task(self):
        if self._get_current_task() is None or self._confirm_action("Exit current task?"):
            if self._get_current_task() is not None:
                self._check_exercise_solved()
            self._open_control_frame()
            self.environment.unload_task()

    def _check_task(self):
        task = self._get_current_task()
        if task is not None:
            if not self.environment.task_manager.is_drawing_complete():
                messagebox.showerror("Status", "Task has not loaded yet.")
                return
            if self.environment.task_manager.validate():
                task.add_user_solution(self.environment.task_manager.get_last_solution())
                self.environment.events.push("reload_task_list")
                if self._confirm_action("CORRECT!\n\nExit from task?", title="Status"):
                    self._open_control_frame()
                    self.environment.unload_task()
                    self._check_exercise_solved()
            else:
                messagebox.showerror("Status", "INCORRECT")

    def _select_definition(self, definition):
        self._current_selected_definition = definition
        self._open_definition_frame()
        self._definition_frame_title.configure(text=self._to_human_name(self._current_selected_definition.title()))
        self._definition_frame_description.configure(text=self._current_selected_definition.description(capitalize=True))

    def _generate_definition_example(self):
        if self._current_selected_definition is not None:
            params = self._current_selected_definition.generate_parameters()
            example = self._current_selected_definition.generate_example(**params)
            if example is not None:
                if not self._current_selected_definition.apply(example, **params):
                    print("generated example does not apply to definition")
                self._definition_frame_description.configure(
                    text=self._current_selected_definition.description(**params))
                if self._current_definition_example_uid is not None:
                    self.environment.erase_graph(self._current_definition_example_uid)
                self._current_definition_example = example
                self._current_definition_example_uid = self.environment.draw_graph(example)

    def _exit_from_definition(self):
        self._current_selected_definition = None
        self._open_control_frame()
        if self._current_definition_example_uid is not None:
            self.environment.erase_graph(self._current_definition_example_uid)
            self._current_definition_example = None
            self._current_definition_example_uid = None

    def _to_human_name(self, title):
        while title and title[0] == ".":
            title = title[1:]
        return title

    def _refresh_task_list(self):
        self._task_listbox_uuids = []
        self._task_listbox.delete(0, "end")
        for uid, task in self.environment.list_tasks().items():
            if not task.anonymous:
                self._task_listbox_uuids.append(uid)
                if task.is_solved():
                    self._task_listbox.insert("end", self._to_human_name(task.name()) + "  [SOLVED]")
                    self._task_listbox.itemconfig("end", {'fg': 'green'})
                else:
                    self._task_listbox.insert("end", self._to_human_name(task.name()))
                    if task.procedural:
                        if task.combined:
                            self._task_listbox.itemconfig("end", {'fg': '#c77'})
                        else:
                            self._task_listbox.itemconfig("end", {'fg': '#777'})

        self._exercise_listbox_tasks = []
        self._exercise_listbox.delete(0, "end")
        if self._current_exercise is not None:
            for task in self._current_exercise.tasks:
                self._exercise_listbox_tasks.append(task)
                if task.is_solved():
                    self._exercise_listbox.insert("end", self._to_human_name(task.name()) + "  [SOLVED]")
                    self._exercise_listbox.itemconfig("end", {'fg': 'green'})
                else:
                    self._exercise_listbox.insert("end", self._to_human_name(task.name()))

    def _refresh_definition_list(self):
        self._definition_listbox_uuids = []
        self._definition_listbox.delete(0, "end")
        for uid, definition in self.environment.list_definitions().items():
            self._definition_listbox_uuids.append(uid)
            self._definition_listbox.insert("end", self._to_human_name(definition.title()))
            if not definition.validated:
                self._definition_listbox.itemconfig("end", {'fg': '#777'})
            elif definition.procedural:
                self._definition_listbox.itemconfig("end", {'fg': '#800'})

    def _refresh_extension_tree(self):
        self._extension_tree.delete(*self._extension_tree.get_children())
        for extension in self.environment.extensions.extensions:
            extension_id = self._extension_tree.insert("", "end", text=extension.filename())
            for category in extension.categories:
                items = extension.get_category(category)
                if len(items) > 0:
                    category_id = self._extension_tree.insert(extension_id, "end", text=category)
                    for item in items:
                        name = item.variable("title")
                        if name is None:
                            name = item.variable("name")
                        self._extension_tree.insert(category_id, "end", text=str(name))
        self._extensions_set_all_expanded(False)

    def _refresh_jobs_mark(self, jobs=None):
        if len(jobs) == 0:
            self._jobs_label.configure(text="No background tasks", fg="#aaa")
            self._jobs_label_spinner.grid_remove()
            self._jobs_label_spinner.stop_animation()
        else:
            if len(jobs) == 1:
                text = jobs[0].title
            else:
                text = "{} background tasks running".format(len(jobs))
            self._jobs_label.configure(text=text, fg="#444")
            self._jobs_label_spinner.start_animation()
            self._jobs_label_spinner.grid(row=0, column=1, sticky="nsew", pady=(3, 0))

    def _init_events(self):
        def ui_wrap(func):
            return lambda **kwargs: self.after(0, lambda *args: func(**kwargs))

        self.environment.events.add_callback("reload_task_list", ui_wrap(self._refresh_task_list))
        self.environment.events.add_callback("reload_definition_list", ui_wrap(self._refresh_definition_list))
        self.environment.events.add_callback("reload_extension_tree", ui_wrap(self._refresh_extension_tree))
        self.environment.events.add_callback("jobs_update", ui_wrap(self._refresh_jobs_mark))

    def _render_loop(self):
        self._running = True
        while self._running:
            self.environment.events.invoke_all()
            self.after(0, lambda: self.container.render())
            sleep(0.05)

    def action(self, action, event):
        coords = event.x, event.y
        if action == "drag":
            coords = max(0, min(self.resolution[0], coords[0])), max(0, min(self.resolution[1], coords[1]))
        uuids = self.container.process_action(action, coords)

    def mainloop(self, **kwargs):
        self._render_thread.start()
        self._start_extension_reload()

        Tk.mainloop(self, **kwargs)
        self._running = False

