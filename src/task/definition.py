import uuid
import re
import time

from graph import *


_registered_definitions = {}


class Definition:
    def __init__(self, register=False):
        self.uuid = str(uuid.uuid4())
        self.registered = register    # definition is registered in global namespace
        self.procedural = False       # definition is generated in runtime
        self.validated = False        # definition was successfully used to create a task
        if register:
            _registered_definitions[self.name()] = self

    def title(self):
        return self.name()

    def name(self):
        raise NotImplementedError()

    def raw_description(self):
        raise NotImplementedError()

    def get_name_aliases(self):
        return {}

    def get_flags(self):
        return ()

    def description(self, capitalize=False, **kwargs):
        aliases = self.get_name_aliases()
        description = self.raw_description()
        description = "No Description Provided" if description is None else description

        while True:
            match = re.search("{{\\s*[A-Za-z_][A-Za-z_$0-9]*\\s*}}", description)
            if match is not None:
                start, end = match.span()
                variable = description[start + 2:end - 2].strip()
                if variable in kwargs:
                    variable = str(kwargs[variable])
                else:
                    if aliases and variable in aliases:
                        variable = aliases[variable]
                    if capitalize:
                        variable = variable.upper()
                description = description[:start] + variable + description[end:]
            else:
                break
        return description

    def can_apply(self, target, **kwargs):
        raise NotImplementedError()

    def apply(self, target, **kwargs):
        raise NotImplementedError()

    def generate_parameters(self):
        raise NotImplementedError()

    def generate_example(self, **kwargs):
        raise NotImplementedError()

    def generate_solution(self, graph, **kwargs):
        raise NotImplementedError()

    def is_valid(self):
        return True


# Wrapper for definition section, defined in extension file
class ExtensionDefinition(Definition):
    def __init__(self, section, manager):
        self.section = section
        self.manager = manager

        Definition.__init__(self, register=True)

    def get_name_aliases(self):
        aliases = self.section.variable("variable_aliases")
        return {} if aliases is None else aliases

    def raw_description(self):
        description = self.section.variable("description")
        return "No Description Provided" if description is None else description

    def get_flags(self):
        flags = self.section.variable("flags")
        if flags is None:
            flags = ()
        else:
            if not isinstance(flags, tuple):
                flags = (flags, )
        return flags

    def title(self):
        title = self.section.variable("title")
        return self.name() if title is None else title

    def name(self):
        name = self.section.variable("name")
        return self.uuid if name is None else name

    def can_apply(self, target, **kwargs):
        if target is None:
            return False
        func = self.section.variable("can_apply")
        return func is None or bool(func(target, **kwargs))

    def apply(self, target, **kwargs):
        if not self.can_apply(target, **kwargs):
            raise RuntimeError("cant apply definition {} for {}".format(self.name(), target))
        func = self.section.variable("apply")
        if func is None:
            raise RuntimeError("definition {} missing apply(target) method".format(self.name()))
        return bool(func(target, **kwargs))

    def generate_parameters(self):
        func = self.section.variable("generate_parameters")
        return {} if func is None else dict(func())

    def generate_example(self, **kwargs):
        func = self.section.variable("generate_example")
        if func is not None:
            example = func(**kwargs)
            if example is not None and not isinstance(example, Graph):
                raise RuntimeError("definition.generate_example must return Graph instance")
            return example

    def generate_solution(self, graph, **kwargs):
        func = self.section.variable("generate_solution")
        if func is not None:
            solution = func(graph.copy(), **kwargs)
            if solution is not None:
                if not isinstance(solution, Graph):
                    raise RuntimeError("definition.generate_example must return Graph instance")
                if not self.apply(solution, **kwargs):
                    raise RuntimeError("generated solution does not apply to its own definition - " + self.name())
            return solution


# Wrapper for any other definition
class DefinitionWrap(Definition):
    def __init__(self, definition, register=False, parameter_filter=lambda x: x, **kwargs):
        self._kwargs = kwargs
        self._wrapped = definition
        self._parameter_filter = parameter_filter
        Definition.__init__(self, register=register)

    def _merge_kwargs(self, kwargs):
        kw = dict(self._kwargs)
        for key, value in kwargs.items():
            key = self._parameter_filter(key)
            if key is not None:
                kw[key] = value
        return kw

    def title(self):
        return self._wrapped.title()

    def name(self):
        return self._wrapped.name()

    def raw_description(self):
        return self._wrapped.raw_description()

    def description(self, capitalize=False, **kwargs):
        return self._wrapped.description(capitalize=capitalize, **self._merge_kwargs(kwargs))

    def get_flags(self):
        return self._wrapped.get_flags()

    def can_apply(self, target, **kwargs):
        return self._wrapped.can_apply(target, **self._merge_kwargs(kwargs))

    def apply(self, target, **kwargs):
        return self._wrapped.apply(target, **self._merge_kwargs(kwargs))

    def generate_parameters(self):
        params = self._wrapped.generate_parameters()
        if params is None:
            return dict(self._kwargs)
        else:
            params = dict(params)
            for key, value in self._kwargs:
                params[key] = value
            return params

    def generate_example(self, **kwargs):
        return self._wrapped.generate_example(**self._merge_kwargs(kwargs))

    def generate_solution(self, graph, **kwargs):
        return self._wrapped.generate_solution(graph, **self._merge_kwargs(kwargs))

    def get_name_aliases(self):
        return self._wrapped.get_name_aliases()

    def is_valid(self):
        return self._wrapped.is_valid()


class DefinitionCombination(Definition):
    def __init__(self, *definitions, register=False, **kwargs):
        # Rearrange into combination tree for more efficient and easy solution (each combination contains 2 definitions)
        if len(definitions) > 2:  # Order is preserved
            if len(definitions) > 3:  # form 2 semi-equal groups: ((...), (...))
                half = len(definitions) // 2
                definitions = (DefinitionCombination(*definitions[:half], **kwargs),
                               DefinitionCombination(*definitions[half:], **kwargs))
            else:  # form one group and one single (a, (b, c))
                definitions = (definitions[0], DefinitionCombination(*definitions[1:], **kwargs))
        if len(definitions) < 2:
            raise RuntimeError("not enough definitions for combination are given")

        self._solved = False
        self._solutions = []
        self._first = DefinitionWrap(definitions[0], parameter_filter=
                                     lambda key: key[3:] if len(key) >= 3 and key[:3] == "$f_" else None)
        self._second = DefinitionWrap(definitions[1], parameter_filter=
                                      lambda key: key[3:] if len(key) >= 3 and key[:3] == "$s_" else None)

        self._initialize(**kwargs)
        Definition.__init__(self, register=register)

    def _initialize(self, cross_solution_iterations=4, name_separator=" ", description_separator="\n"):
        self._cross_solution_iterations = cross_solution_iterations
        self._name_separator = name_separator
        self._description_separator = description_separator

    # tries to find a solution for definition combination, kwargs is list of fixed params, that wont be randomized
    # for each successful try adds variation of parameters
    def solve(self, tries=2, scheduler=None, **kwargs):
        a, b = self._first, self._second
        for i in range(tries):
            params = dict(self.generate_random_parameters())
            for key, value in kwargs.items():
                params[key] = value
            example = a.generate_example(**params)
            if example is not None:
                solution = self._cross_solve(example, _scheduler=scheduler,
                                             _iterations=self._cross_solution_iterations, **params)
                if solution is not None:
                    # assert self.apply(solution, **params)
                    self._solutions.append((params, example, solution))
                    self._solved = True
                a, b = b, a
            if scheduler:
                scheduler.yield_()
        return self._solved

    # try to find solution for given graph continuously fixing one solution with another,
    # O(N) = I^(N*ln(N)) where I = _iterations
    def _cross_solve(self, solution, _iterations=1, _scheduler=None, **params):
        a, b = self._first, self._second
        if not a.can_apply(solution, **params) or not a.apply(solution, **params):
            solution = a.generate_solution(solution, **params)
        if solution is None:
            return
        if _scheduler:
            _scheduler.yield_()
        for i in range(_iterations):
            if b.can_apply(solution, **params) and b.apply(solution, **params):
                return solution
            solution = b.generate_solution(solution, **params)
            if solution is None:
                return
            a, b = b, a
            if _scheduler:
                _scheduler.yield_()

    def title(self):
        return self._first.title() + self._name_separator + self._second.title()

    def name(self):
        return self._first.name() + "_" + self._second.name()

    def raw_description(self):
        return "This should not appear"

    def description(self, capitalize=False, **kwargs):
        return self._first.description(capitalize=capitalize, **kwargs) + self._description_separator + \
               self._second.description(capitalize=capitalize, **kwargs)

    def get_flags(self):
        return self._first.get_flags() + self._second.get_flags()

    def can_apply(self, target, **kwargs):
        return self._first.can_apply(target, **kwargs) and self._second.can_apply(target, **kwargs)

    def apply(self, target, **kwargs):
        return self._first.apply(target, **kwargs) and self._second.apply(target, **kwargs)

    def generate_random_parameters(self):
        params = {}
        generated = self._first.generate_parameters()
        if generated:
            for key, value in generated.items():
                params["$f_" + key] = value
        generated = self._second.generate_parameters()
        if generated:
            for key, value in generated.items():
                params["$s_" + key] = value
        return params

    def generate_parameters(self):
        if len(self._solutions) > 0:
            params, example, solution = self._solutions[random.randint(0, len(self._solutions) - 1)]
            return params
        return {}

    def generate_example(self, **kwargs):
        generated = self._cross_solve(self._first.generate_example(**kwargs),
                                      _iterations=self._cross_solution_iterations, **kwargs)
        if generated is not None:
            assert self.apply(generated, **kwargs)
        if generated is None:
            print("restoring example for", kwargs)
            for params, example, solution in self._solutions:
                if kwargs == params:
                    print("restored")
                    return solution
        return generated

    def generate_solution(self, graph, **kwargs):
        return self._cross_solve(graph, _iterations=self._cross_solution_iterations, **kwargs)

    def get_name_aliases(self):
        aliases = {}
        generated = self._first.get_name_aliases()
        if generated:
            for key, value in generated.items():
                aliases["$f_" + key] = value
        generated = self._second.get_name_aliases()
        if generated:
            for key, value in generated.items():
                aliases["$s_" + key] = value
        return aliases

    def is_valid(self):
        return self._solved and self._first.is_valid() and self._second.is_valid()


def make_definition(*names, **kwargs):
    definitions = []
    for name in names:
        if name in _registered_definitions:
            definitions.append(_registered_definitions[name])
        else:
            raise RuntimeError("no definition for " + name)

    if len(definitions) == 1:
        combined = definitions[0]
    else:
        combined = DefinitionCombination(*definitions)
    if len(kwargs) > 0:
        return DefinitionWrap(combined, **kwargs)
    else:
        return combined

