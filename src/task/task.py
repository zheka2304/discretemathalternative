import uuid
from threading import *

from visualization import *
from graph import *


class TaskManager:
    def __init__(self, visualizer, manager, message_handle):
        self.visualizer = visualizer
        self.manager = manager
        self.message_handle = message_handle

        self._lock = Lock()
        self._task = None
        self._condition = None
        self._last_solution = None
        self._visualization_id = None

    def get_current_task(self):
        return self._task

    def get_current_condition(self):
        return self._condition

    def get_last_solution(self):
        return self._last_solution

    def unload(self):
        with self._lock:
            if self._visualization_id is not None:
                self.visualizer.erase(self._visualization_id)
            if self._task is not None:
                self._task = None
                self._condition = None
                self._last_solution = None
                self._visualization_id = None

    def load(self, task):
        self.unload()
        with self._lock:
            try:
                self._condition = task.generate_condition()
            except Exception as err:
                self.message_handle.report(message="error in generating task condition", exception=err)
                return
            self._task = task
            self._visualization_id = str(uuid.uuid4())
            self.visualizer.draw(self._condition, task, self._visualization_id)

    def show_solution(self, solution):
        self.unload()
        with self._lock:
            self._visualization_id = str(uuid.uuid4())
            self.visualizer.draw(solution, None, self._visualization_id, reset=False)

    def is_drawing_complete(self):
        return self._visualization_id is None or self.visualizer.is_complete(self._visualization_id)

    def update(self):
        with self._lock:
            if self._task is not None and self._condition is not None:
                try:
                    self._task.update(self._condition)
                except Exception as err:
                    self.message_handle.report(message="error in updating task", exception=err)

    def get_current(self):
        with self._lock:
            if self._task is not None:
                try:
                    return self._task.convert(self._condition)
                except Exception as err:
                    self.message_handle.report(message="error in converting solution", exception=err)

    def validate(self):
        condition = self.get_current()
        with self._lock:
            if self._task is not None:
                try:
                    result = self._task.validate(condition, original=self._condition.copy())
                except Exception as err:
                    self.message_handle.report(message="error in validating solution", exception=err)
                    return False
                if result:
                    self._last_solution = self._condition.copy()
                return result


class TaskVisualizerMessageUnit(InterfaceUnit):
    def initialize(self):
        self.add_values(
            last_text=None,
            text=None
        )

    def refresh(self):
        if self.last_text != self.text:
            self.invalidate()

    def render(self, canvas, transform):
        if self.text is not None:
            x, y = transform(0.5, 0.5)
            canvas.create_text(x, y, text=self.text, tag=self.tag(), font=("", 32), fill="#999")
        self.last_text = self.text

    def event(self, name, position):
        pass


class TaskVisualizer:
    def __init__(self, container, screen=(0, 0, 1, 1)):
        self.container = container
        self.screen = screen
        self.rendered = {}
        self.erased = []
        self.complete = []

        self.debug_window = None
        self.job_manager = None
        self._interface_text = None
        self._interface_group = None

    def _initialize_message_if_required(self):
        if self._interface_text is None:
            self._interface_text = TaskVisualizerMessageUnit(None)
            self._interface_group = InterfaceUnitGroup()
            self._interface_group.add(self._interface_text)
            self._interface_group.screen(self.screen)
            self.container.add(self._interface_group)

    def is_complete(self, uid):
        return uid in self.complete

    def set_message(self, message):
        self._initialize_message_if_required()
        self._interface_text.text = message

    def run(self, func):
        if self.job_manager is not None:
            self.job_manager.add(func, family="draw_graph", title="Building Graph")
        else:
            func()

    def erase(self, uid, nodelete=False):
        self.erased.append(uid)
        if uid in self.rendered:
            unit_group = self.rendered[uid].get_unit_group()
            self.container.remove(unit_group)
            if not nodelete:
                del self.rendered[uid]
            if self.debug_window is not None:
                self.debug_window.clear()

    def draw(self, graph, task, uid, setup=None, reset=True):
        if setup is None:
            kwargs = pack_kwargs(equilibrium_delta=0, equilibrium_timeout=10000,
                                 border=(0, 0, 1, 1), t=1e-10, d0=1e-10)
            setup = [
                pack_kwargs(rate=.025, frame_limit=750, factors=(0, 1e3, 1e1, 0), **kwargs),  # minimize edge crossings
                pack_kwargs(rate=0, frame_limit=1, factors=(0, 0, 1e3, 0), **kwargs),       # calculate final energy
            ]

        def draw(progress=None, **kwargs):
            if self.debug_window is not None:
                self.debug_window.clear()

            if uid in self.erased:
                return

            if uid in self.rendered:
                if graph is not self.rendered[uid].graph:
                    raise RuntimeError("different graphs are drawn with same uuid: " + uid)
                self.erase(uid, nodelete=True)
                rendered = self.rendered[uid]
            else:
                rendered = self.rendered[uid] = RenderedGraph(graph)

            self.set_message("Loading")
            if reset:
                graph.reset_vertex_positions()
            rendered.build(setup, epochs=1, debug_window=self.debug_window)
            group = rendered.get_unit_group()
            group.screen(self.screen)
            if uid not in self.erased:
                group.assign_to(task)
                self.container.add(group)

            if self.debug_window is not None:
                self.debug_window.clear()

            self.complete.append(uid)
            self.set_message(None)

        self.run(draw)

