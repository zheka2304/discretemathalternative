import re
import json
import graph.saver as graph_saver


class ParserException(Exception):
    def __init__(self, *args, index=-1, source=None):
        Exception.__init__(self, *args)
        self.index = index
        self.source = source

    def __str__(self):
        message = Exception.__str__(self)
        if self.source is not None:
            if self.index != -1:
                index = self.index
                error_line = None
                for line in self.source.split("\n"):
                    if index <= len(line):
                        error_line = line
                        break
                    index -= len(line) + 1
                if error_line is not None:
                    prefix = __name__ + "."
                    message += "\n" + " " * len(prefix) + error_line + "\n" + " " * len(prefix) + " " * index + "^"
            else:
                message += "\n" + self.source
        return message


section_types = {}


def new_section(name, **kwargs):
    if name in section_types:
        return section_types[name](**kwargs)


def find_word(string, offset):
    group = re.match("(?P<word>^[a-zA-Z_][a-zA-Z_0-9$]*)", string[offset:])
    if group is not None:
        return group.group("word")


def skip_spaces(string, offset):
    group = re.match("(?P<space>^\s+)", string[offset:])
    if group is not None:
        return offset + group.end()
    else:
        return offset


def section_class(name):
    def decorate(clazz):
        section_types[name.lower()] = clazz
        return clazz
    return decorate


class Section:
    def __init__(self):
        self.subsections = []
        self.variables = {}

    def variable(self, name):
        if name in self.variables:
            return self.variables[name]

    def sections(self, clazz):
        sections = []
        for section in self.subsections:
            if isinstance(section, clazz):
                sections.append(section)
        return sections

    def subsection(self, clazz):
        sections = self.sections(clazz)
        if len(sections) > 0:
            return sections[0]

    def parse(self, contents):
        raise NotImplementedError()

    def initialize(self, namespace, **kwargs):
        raise NotImplementedError()

    def action(self, name, **kwargs):
        raise NotImplementedError()

    def __repr__(self):
        variables = []
        for name, value in self.variables.items():
            variables.append(name + "=" + str(value))
        subs = ""
        for section in self.subsections:
            for line in repr(section).split("\n"):
                subs += "\n  " + line
        return self.__class__.__name__ + "(" + ", ".join(variables) + ")" + subs


class ParametrizedSection(Section):
    def __init__(self):
        Section.__init__(self)
        self._variables_initialized = False

    def valid_subsection(self, name, section):
        return True

    def valid_variable(self, name, value):
        return True

    def validate(self):
        pass

    def parse(self, contents):
        index = 0
        while index < len(contents):
            index = skip_spaces(contents, index)
            word = find_word(contents, index)
            if word is not None:
                index = skip_spaces(contents, index + len(word))
                if contents[index] == "{":
                    braces = 1
                    index += 1
                    start = index
                    while True:
                        if index >= len(contents):
                            raise ParserException("missing }", index=start, source=contents)
                        if contents[index] == "{":
                            braces += 1
                        if contents[index] == "}":
                            braces -= 1
                        if braces <= 0:
                            break
                        index += 1
                    subsection = new_section(word)
                    if subsection is None:
                        raise ParserException("invalid section name: " + word, index=start, source=contents)
                    subsection.parse(contents[start:index])
                    if not self.valid_subsection(word, subsection):
                        raise ParserException("invalid subsection: " + word)
                    self.subsections.append(subsection)
                    index += 1
                elif contents[index] == "=":
                    try:
                        index += 1
                        start = index
                        while contents[index] not in ("\n", ";"):
                            index += 1
                        self.variables[word] = contents[start:index]
                    except Exception as err:
                        raise ParserException(str(err), index=index, source=contents)
                else:
                    raise ParserException("block or variable expected", index=index, source=contents)
            else:
                if index < len(contents):
                    if contents[index] == "}":
                        raise ParserException("unexpected }", index=index, source=contents)
                    else:
                        raise ParserException("keyword expected", index=index, source=contents)

    def initialize(self, namespace, **kwargs):
        for section in self.subsections:
            section.initialize(namespace, **kwargs)
        if not self._variables_initialized:
            for key, value in self.variables.items():
                self.variables[key] = eval(value, dict(namespace))
            self._variables_initialized = True
        for key, value in self.variables.items():
            if not self.valid_variable(key, value):
                raise ParserException("invalid variable " + key + " = '" + str(value) + "' in section " +
                                      self.__class__.__name__)
        self.validate()

    def action(self, name, target=None, **kwargs):
        for section in self.subsections:
            if target is None or isinstance(section, target):
                section.action(name, **kwargs)

    def search(self, checker):
        if checker(self):
            return self
        for section in self.subsections:
            if checker(section):
                return section
            if isinstance(section, ParametrizedSection):
                found = section.search(checker)
                if found:
                    return found

    def search_all(self, checker):
        if checker(self):
            return {self}
        found = set()
        for section in self.subsections:
            if checker(section):
                found.add(section)
            if isinstance(section, ParametrizedSection):
                for element in section.search_all(checker):
                    found.add(element)
        return found


@section_class("module")
class SectionModule(ParametrizedSection):
    name = None

    def parse(self, contents):
        ParametrizedSection.parse(self, contents)
        self.name = self.variable("name")
        if self.name is not None:
            value = eval(self.name).strip()
            if isinstance(value, str) and find_word(value, 0) == value:
                self.name = value
            else:
                raise ParserException("invalid module name: " + self.name)
        else:
            raise ParserException("missing module name")

    def valid_subsection(self, name, section):
        return name in ("python", "import")


@section_class("graph")
class SectionGraph(ParametrizedSection):
    def __init__(self):
        ParametrizedSection.__init__(self)
        self.name = None
        self.data = None

    def validate(self):
        self.name = self.variable("name")
        if not self.name:
            raise ParserException("graph section missing name")
        data = self.variable("data")
        try:
            self.data = json.loads(str(data))
        except json.JSONDecodeError as e:
            raise ParserException("failed to parse graph json: " + str(e))
        graph_saver.register_graph_data(self.name, self.data)

    def valid_subsection(self, name, section):
        return False


@section_class("task")
class SectionTask(ParametrizedSection):
    def valid_subsection(self, name, section):
        return name not in ("module", "task")


@section_class("action")
class SectionAction(ParametrizedSection):
    def valid_subsection(self, name, section):
        return name in ("python", )

    def validate(self):
        action_set = self.variable("action_set")
        if action_set and isinstance(action_set, dict):
            for name in action_set:
                if name not in self.variables:
                    self.variables[name] = action_set[name]


@section_class("solution")
class SectionSolution(ParametrizedSection):
    def valid_subsection(self, name, section):
        return name in ("python", )


@section_class("definition")
class SectionDefinition(ParametrizedSection):
    def validate(self):
        name = self.variable("name")
        if name is not None:
            if not (isinstance(name, str) and find_word(name, 0) == name):
                raise ParserException("invalid definition name: " + name)
        else:
            raise ParserException("missing definition name")

    def valid_subsection(self, name, section):
        return name in ("import", "python")

    def can_apply(self, obj):
        can_apply = self.variable("can_apply")
        if can_apply is not None:
            return can_apply(obj)
        else:
            return True

    def apply(self, obj):
        apply = self.variable("apply")
        if apply is not None:
            return apply(obj)
        else:
            raise ParserException("definition has no apply method!")

    def get_name(self):
        return self.variable("name")


@section_class("import")
class SectionImport(Section):
    def __init__(self):
        Section.__init__(self)
        self.imports = []

    def parse(self, contents):
        for name in re.split("[^a-zA-Z0-9_$]+", contents):
            name = name.strip()
            if len(name) > 0:
                self.imports.append(name)

    def initialize(self, namespace, manager=None, extension=None, **kwargs):
        for name in self.imports:
            if not manager.import_module(name, namespace, caller=extension):
                raise ImportError("extension module not found: " + name)

    def action(self, name, **kwargs):
        pass


@section_class("python")
class SectionPython(Section):
    def __init__(self):
        Section.__init__(self)
        self.compiled = None
        self.result = None

    def parse(self, contents):
        lines = []
        intend = len(contents)
        for line in contents.split("\n"):
            line = line.replace("\t", "    ")
            if len(line.strip()) > 0:
                current = 0
                while current < len(line) and line[current] == " ":
                    current += 1
                if current < intend:
                    intend = current
                lines.append(line)

        source = ""
        for line in lines:
            source += line[intend:] + "\n"

        self.compiled = compile(source, "<string>", "exec")

    def initialize(self, namespace, **kwargs):
        self.result = exec(self.compiled, namespace)

    def action(self, name, **kwargs):
        pass

