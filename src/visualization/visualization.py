import random
from time import perf_counter, sleep
from math import *
from .util import *
from .unit import *
from .debug import *


class VertexSimulation:
    def __init__(self):
        self.vertices = []  # simulation
        self.vector = []    # all coordinates
        self.edges = set()

        self._energy = None
        self._strategy = None
        self._lowest_energy = None
        self._lowest_state = None
        self._equilibrium_state = None
        self._equilibrium_frames = 0

    def add_vertex(self, vertex, position=(0, 0)):
        self.vertices.append(vertex)
        self.vector.append(position[0])
        self.vector.append(position[1])
        for edge in vertex.edges:
            self.edges.add(edge)

    def system_energy(self, factors=(1,) * 4, d0=0.001, **kwargs):

        edge_length = 0
        for i in range(len(self.vector) // 2):
            x1, y1 = self.vector[i * 2], self.vector[i * 2 + 1]
            for j in range(i + 1, len(self.vector) // 2):
                x2, y2 = self.vector[j * 2], self.vector[j * 2 + 1]
                edge_length += 1 / max(1e-10, segment_length_sqr(x1, y1, x2, y2))

        edges = list(self.edges)
        edge_intersection = 0
        edge_distance = 0
        edge_angle = 0

        for n in range(len(edges)):
            edge1 = edges[n]
            i = self.vertices.index(edge1.start)
            j = self.vertices.index(edge1.end)
            x1, y1 = self.vector[i * 2], self.vector[i * 2 + 1]
            x2, y2 = self.vector[j * 2], self.vector[j * 2 + 1]

            intersects = []
            for m in range(n + 1, len(edges)):
                edge2 = edges[m]
                k = self.vertices.index(edge2.start)
                l = self.vertices.index(edge2.end)
                x3, y3 = self.vector[k * 2], self.vector[k * 2 + 1]
                x4, y4 = self.vector[l * 2], self.vector[l * 2 + 1]
                if edge1.start not in edge2 and edge1.end not in edge2:
                    intersection = segment_intersect(x1, y1, x2, y2, x3, y3, x4, y4)
                    if intersection is not None:
                        intersects.append(edge2.start)
                        intersects.append(edge2.end)
                        xi, yi = intersection

                        xt1, yt1 = (x1, y1) if len(edge1.start.edges) > len(edge1.end.edges) else (x2, y2)
                        xt2, yt2 = (x3, y3) if len(edge2.start.edges) > len(edge2.end.edges) else (x4, y4)
                        delta1 = (abs(len(edge1.start.edges) - len(edge1.end.edges)) + 1) ** 2
                        delta2 = (abs(len(edge2.start.edges) - len(edge2.end.edges)) + 1) ** 2

                        edge_intersection -= (min(segment_length_sqr(xi, yi, xt1, yt1),
                                                  segment_length_sqr(xi, yi, xt1, yt1)) ** .5) * delta1 + \
                                             (min(segment_length_sqr(xi, yi, xt2, yt2),
                                                  segment_length_sqr(xi, yi, xt2, yt2)) ** .5) * delta2

                    len_sqr = segment_length_sqr(x1, y1, x2, y2) * segment_length_sqr(x3, y3, x4, y4)
                    if len_sqr > 0:
                        edge_angle += 1 / max(1e-10, 1 - abs(((x2 - x1) * (x4 - x3) + (y2 - y1) * (y4 - y3)) / sqrt(len_sqr))) ** 3

            for k in range(len(self.vertices)):
                vertex = self.vertices[k]
                if vertex not in edge1:
                    if vertex in intersects:
                        edge_distance += 1 / d0
                    else:
                        x3, y3 = self.vector[k * 2], self.vector[k * 2 + 1]
                        edge_distance += 1 / max(d0, segment_distance_sqr(x1, y1, x2, y2, x3, y3))

        return factors[0] * edge_length + factors[1] * edge_intersection + \
               factors[2] * edge_distance + edge_angle * factors[3]

    def normalize(self, verbose=False, size=1):
        vector = self.vector
        bounds = [0, 0, 0, 0]
        if len(vector) > 0:
            bounds[0] = bounds[2] = vector[0]
            bounds[1] = bounds[3] = vector[1]
        for i in range(len(vector) // 2):
            x, y = vector[i * 2], vector[i * 2 + 1]
            if bounds[0] > x:
                bounds[0] = x
            if bounds[1] > y:
                bounds[1] = y
            if bounds[2] < x:
                bounds[2] = x
            if bounds[3] < y:
                bounds[3] = y
        if verbose:
            print("normalize bounds", bounds)
        for i in range(len(vector) // 2):
            vector[i * 2] = (vector[i * 2] - bounds[0]) / (bounds[2] - bounds[0]) * size
            vector[i * 2 + 1] = (vector[i * 2 + 1] - bounds[1]) / (bounds[3] - bounds[1]) * size

    def simulate_frame(self, rate=1, deviation=1, t=1, debug_window=None, **kwargs):
        if self._energy is None:
            self._energy = self.system_energy(**kwargs)
            self._lowest_energy = self._energy
            self._lowest_state = self.vector[:]
            self._equilibrium_state = self._energy

        last = self.vector[:]
        for index in range(len(self.vector)):
            if self.vertices[index // 2].position is None:  # if not fixed
                self.vector[index] += random.gauss(deviation, 1) * rate
        self.normalize()

        energy = self.system_energy(verbose=True, **kwargs)
        if energy < self._lowest_energy:
            self._lowest_energy = energy
            self._lowest_state = self.vector[:]

        if energy < self._energy:
            self._energy = energy
            if debug_window is not None:
                debug_window.draw(self.edges, self.vector)
        else:
            if random.random() < exp(-(energy - self._energy) / t):
                self._energy = energy
            else:
                self.vector = last

    def simulate(self, equilibrium_timeout=50, equilibrium_delta=1e3, frame_limit=1000, debug=None, **kwargs):
        frame = 0
        self._energy = None
        self._strategy = None
        self._equilibrium_state = None
        self._equilibrium_frames = 0

        while frame < frame_limit and self._equilibrium_frames < equilibrium_timeout:
            self.simulate_frame(**kwargs)
            if self._energy is not None:
                if self._energy + equilibrium_delta < self._equilibrium_state:
                    self._equilibrium_state = self._energy
                    self._equilibrium_frames = 0
                else:
                    self._equilibrium_frames += 1
            if debug is not None:
                debug(self._energy, frame=frame, state=self._equilibrium_state)
            frame += 1
        self.vector = self._lowest_state

        return self._lowest_energy

    def get_state(self):
        positions = []
        for i in range(len(self.vector) // 2):
            positions.append((self.vertices[i], self.vector[i * 2], self.vector[i * 2 + 1]))
        return positions

    def set_state(self, state):
        vector = []
        for i in range(len(state)):
            vector.append(state[i][0])
            vector.append(state[i][1])
        self.vector = vector

    def fix_graph_paths(self):
        pass

    def simulate_physics(self, attraction=1e-2, repulsion=1, edge_repulsion=5e0, edge_length=.25, frames=2000, debug_window=None, **kwargs):
        self.normalize(size=10)

        state = []  # each vertex is [x, y, vx, vy, m=1]
        for i in range(len(self.vector) // 2):
            x, y = self.vector[i * 2], self.vector[i * 2 + 1]
            state.append([x, y, 0, 0, 1])

        rate = 5e-4

        def force(vertex, _fx, _fy):
            vertex[2] += _fx / vertex[4] * rate
            vertex[3] += _fy / vertex[4] * rate

        draw_time_skip = 0
        for frame in range(frames):
            if debug_window is not None and perf_counter() > draw_time_skip:
                draw_time_skip = perf_counter() + debug_window.draw_physics(self.edges, state) * 4  # TODO: adjust

            # apply repulsion
            for i in range(len(state)):
                vertex1 = state[i]
                x1, y1 = vertex1[0], vertex1[1]
                for j in range(i + 1, len(state)):
                    vertex2 = state[j]
                    x2, y2 = vertex2[0], vertex2[1]
                    d2 = segment_length_sqr(x1, y1, x2, y2)
                    fx, fy = (x1 - x2) * repulsion / d2 ** 1.5, (y1 - y2) * repulsion / d2 ** 1.5
                    force(vertex1, fx, fy)
                    force(vertex2, -fx, -fy)

            # apply attraction
            for edge in self.edges:
                vertex1 = state[edge.start.index]
                vertex2 = state[edge.end.index]
                x1, y1 = vertex1[0], vertex1[1]
                x2, y2 = vertex2[0], vertex2[1]
                d = segment_length_sqr(x1, y1, x2, y2) ** .5
                fx, fy = (x2 - x1) * attraction * (d - edge_length) / d, (y2 - y1) * attraction * (d - edge_length) / d
                force(vertex1, fx, fy)
                force(vertex2, -fx, -fy)

                # repulse from edges
                for vertex in state:
                    if vertex != vertex1 and vertex != vertex2:
                        x3, y3 = vertex[0], vertex[1]
                        xi, yi, ti = segment_distance_sqr(x1, y1, x2, y2, x3, y3, return_intersect=True)
                        if abs(ti) > 1e-10 and abs(ti - 1) > 1e-10:
                            d2 = segment_length_sqr(xi, yi, x3, y3)
                            k = (d2 ** .5) / d
                            ratio = .2
                            if k < ratio:
                                k = 1 / (k / ratio)
                            else:
                                k = 0
                            k = min(1e-1 * d / edge_repulsion, k)
                            fx, fy = (x3 - xi) * k * edge_repulsion / d2 ** .5, (y3 - yi) * k * edge_repulsion / d2 ** .5
                            force(vertex, fx, fy)
            # move
            friction = .99
            velocity = 1
            for i in range(len(state)):
                vertex = state[i]
                if self.vertices[i].position is None:  # if not fixed
                    vertex[0] += vertex[2] * velocity
                    vertex[1] += vertex[3] * velocity
                vertex[2] *= friction
                vertex[3] *= friction

        for i in range(len(state)):
            self.vector[i * 2] = state[i][0]
            self.vector[i * 2 + 1] = state[i][1]
        self.normalize(verbose=False)

    def apply_state(self, indices=None):
        self.normalize()
        for i in range(len(self.vertices)):
            if indices is None or i in indices:
                self.vertices[i].position = self.vector[i * 2], self.vector[i * 2 + 1]

    def minimize_intersections(self):
        count = 1
        frame = 0
        while count > 0 and frame < 10:
            count = 0
            frame += 1
            for edge1 in self.edges:
                for edge2 in self.edges:
                    if edge1.start not in edge2 and edge1.end not in edge2:
                            x1, y1 = self.vector[edge1.start.index * 2], self.vector[edge1.start.index * 2 + 1]
                            x2, y2 = self.vector[edge1.end.index * 2], self.vector[edge1.end.index * 2 + 1]
                            x3, y3 = self.vector[edge2.start.index * 2], self.vector[edge2.start.index * 2 + 1]
                            x4, y4 = self.vector[edge2.end.index * 2], self.vector[edge2.end.index * 2 + 1]
                            if segment_intersect(x1, y1, x2, y2, x3, y3, x4, y4):
                                xi, yi, t = segment_distance_sqr(x3, y3, x4, y4, x1, y1, return_intersect=True)
                                d = sqrt((xi - x1) ** 2 + (yi - y1) ** 2)
                                if d == 0:
                                    d = 1
                                xi -= (x1 - xi) / d * 0.2
                                yi -= (y1 - y1) / d * 0.2
                                self.vector[edge1.start.index * 2], self.vector[edge1.start.index * 2 + 1] = xi, yi
                                count += 1
        print("intersections", count)


class RenderedGraph:
    def __init__(self, graph):
        self.graph = graph
        self._render_group = InterfaceUnitGroup()

    def build(self, sequence, seed=None, epochs=1, position=(0, 0, 1, 1), debug_window=None):
        if seed is not None:
            random.seed(seed)

        best_simulation, lowest_energy = None, None
        for epoch in range(epochs):
            simulation = VertexSimulation()
            for vertex in self.graph:
                w2, h2 = (position[2] - position[0]) / 2, (position[3] - position[1]) / 2
                if vertex.position is None:
                    simulation.add_vertex(vertex, position=(
                        w2 * random.random() + position[0],
                        h2 * random.random() + position[1]
                    ))
                else:
                    simulation.add_vertex(vertex, position=vertex.position)

            energy = 0
            for kwargs in sequence:
                if not self.graph.nothing_to_simulate():
                    energy = simulation.simulate(debug_window=debug_window, **kwargs)

            if debug_window is not None:
                debug_window.clear()
            if best_simulation is None or energy < lowest_energy:
                best_simulation, lowest_energy = simulation, energy

        if not self.graph.nothing_to_simulate():
            best_simulation.simulate_physics(debug_window=debug_window)
            best_simulation.apply_state()
        vertices = best_simulation.get_state()

        borders = [0, 0, 1, 1]
        self._render_group = InterfaceUnitGroup()
        self._render_group.add(InterfaceUnitSheet(self.graph))

        edges = set()
        vertex_search = []
        for vertex, x, y in vertices:
            vertex_search.append(vertex)
            self._render_group.add(InterfaceUnitVertex(vertex, position=(
                (x - borders[0]) / (borders[2] - borders[0]),
                (y - borders[1]) / (borders[3] - borders[1])
            )))
            for edge in vertex.edges:
                edges.add(edge)

        for edge in edges:
            v1, x1, y1 = vertices[vertex_search.index(edge.start)]
            v2, x2, y2 = vertices[vertex_search.index(edge.end)]

            self._render_group.add(InterfaceUnitEdge(edge, start=(
                (x1 - borders[0]) / (borders[2] - borders[0]),
                (y1 - borders[1]) / (borders[3] - borders[1])
            ), end=(
                (x2 - borders[0]) / (borders[2] - borders[0]),
                (y2 - borders[1]) / (borders[3] - borders[1])
            )))

    def get_unit_group(self):
        return self._render_group

