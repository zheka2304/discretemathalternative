import uuid
from math import *

from .util import *
from .graphics import *


class Transform:
    def __init__(self, parent=None, scale=(1, 1), translate=(0, 0)):
        self.parent = parent
        self._scale = scale
        self._translate = translate

    # set (not add) translation to transform
    def translate(self, *args):
        assert len(args) < 3
        self._translate = args if len(args) == 2 else args[0], args[0]

    # set (not add) scale to transform
    def scale(self, *args):
        assert len(args) < 3
        self._scale = args if len(args) == 2 else args[0], args[0]

    # set screen transformation
    def screen(self, src, dst):
        self._scale = ((dst[2] - dst[0]) / (src[2] - src[0]), (dst[3] - dst[1]) / (src[3] - src[1]))
        self._translate = (dst[0] - src[0] * self._scale[0], dst[1] - src[1] * self._scale[1])

    def transform(self, pos):
        pos = (pos[0] * self._scale[0] + self._translate[0], pos[1] * self._scale[1] + self._translate[1])
        if self.parent is not None:
            pos = self.parent.transform(pos)
        return pos

    def transform_back(self, pos):
        if self.parent is not None:
            pos = self.parent.transform_back(pos)
        pos = ((pos[0] - self._translate[0]) / self._scale[0], (pos[1] - self._translate[1]) / self._scale[1])
        return pos

    def invert(self):
        self._translate = (-self._translate[0] / self._scale[0], -self._translate[1] / self._scale[1])
        self._scale = (1 / self._scale[0], 1 / self._scale[1])
        return self

    def copy(self):
        return Transform(self.parent, translate=self._translate, scale=self._scale)

    def deepcopy(self):
        parent = None if self.parent is None else self.parent.deepcopy()
        return Transform(parent, translate=self._translate, scale=self._scale)

    def __call__(self, *args, **kwargs):
        if len(args) == 1:
            return self.transform(*args)
        else:
            return self.transform(args)


class InterfaceUnit:
    def __init__(self, obj, **kwargs):
        self.obj = obj
        self._uuid = str(uuid.uuid4())
        self.dirty = True
        self._kwargs = kwargs

        self.highlighted = False
        self.layer = 0
        self.order = 0
        self.group = None

        self.initialize()

    def task(self):
        if self.group is not None:
            return self.group.task()

    def uuid(self):
        return self._uuid

    def tag(self):
        return "unit$" + self.uuid()

    def initialize(self):
        pass

    def add_values(self, **kwargs):
        for key, value in kwargs.items():
            if key not in self._kwargs:
                self._kwargs[key] = value

    def set_order(self, order):
        self.order = order
        self.invalidate()

    def __getattr__(self, item):
        if item in ("obj", "dirty", "_kwargs", "_uuid"):
            return object.__getattr__(self, item)
        else:
            return self._kwargs[item]

    def __setattr__(self, key, value):
        if key in ("obj", "dirty", "_kwargs", "_uuid"):
            object.__setattr__(self, key, value)
        else:
            self._kwargs[key] = value

    def erase(self, canvas):
        canvas.delete(self.tag())

    def redraw_if_dirty(self, canvas, transform):
        if self.dirty:
            canvas.delete(self.tag())
            self.render(canvas, transform)
            self.dirty = False

    def refresh(self):
        raise NotImplementedError()

    def render(self, canvas, transform):
        raise NotImplementedError()

    def action(self, name, position):
        if name == "highlight":
            self.highlighted = True
        elif name == "un-highlight":
            self.highlighted = False
        else:
            self.event(name, position)
        self.invalidate()

    def event(self, name, position):
        raise NotImplementedError()

    # return click priority or -1 if not clicked
    def process_click(self, pos):
        return None

    def invalidate(self):
        self.dirty = True


class InterfaceUnitVertex(InterfaceUnit):
    def initialize(self):
        self.layer = 2
        self.add_values(
            radius=0.015,
            check_distance=0.03,
            selected=False,
            color=0,
            palette="default",
            info=""
        )

    def _set_selected(self, selected):
        self.selected = selected
        self.obj.active = self.selected
        self.set_order(1 if self.selected else 0)

    def refresh(self):
        self._set_selected(self.obj.active)
        if self.color != self.obj.color:
            self.color = self.obj.color
            self.invalidate()
        if self.position != self.obj.position:
            self.position = self.obj.position
            self.invalidate()
        info = self.obj.get_temp_value("t_info", "")
        if self.info != info:
            self.info = info
            self.invalidate()

    def render(self, canvas, transform):
        x, y = self.position
        x1, y1 = transform(x - self.radius, y - self.radius)
        x2, y2 = transform(x + self.radius, y + self.radius)
        canvas.create_oval(x1, y1, x2, y2,
                           fill=get_color_from_palette("vertex_foreground", self.palette, self.color) if self.selected else "white",
                           width=2 if self.highlighted else 1,
                           outline=get_color_from_palette("vertex_highlight", self.palette, self.color) if self.highlighted else "gray",
                           tag=self.tag())

        if self.info:
            canvas.create_text(*transform(x + self.radius * 2, y - self.radius * 2), text=self.info, tag=self.tag())

    def event(self, name, position):
        task = self.task()
        if task is not None:
            bindings = {
                "left": "vertex_select",
                "right": "vertex_switch",
                "drag": "vertex_drag",
                "double": "vertex_double"
            }
            if name in bindings:
                task.action(bindings[name], self.obj, self, position=position, task=task)

    def process_click(self, pos):
        dist = max(1e-6, segment_length_sqr(*pos, *self.position))
        if dist < self.check_distance ** 2:
            return 3, 1 / dist
        return None


class InterfaceUnitEdge(InterfaceUnit):
    def initialize(self):
        self.layer = 1
        self.add_values(
            check_distance=0.025,
            step=25,
            arrow_size=0.0275,
            vertex_radius=0.015,
            directed=False,
            selected=False,
            color=0,
            palette="default",
            info=""
        )

    def _set_selected(self, selected):
        self.selected = selected
        self.obj.active = self.selected
        self.set_order(1 if self.selected else 0)

    def refresh(self):
        self._set_selected(self.obj.active)
        if self.color != self.obj.color:
            self.color = self.obj.color
            self.invalidate()
        if self.start != self.obj.start.position:
            self.start = self.obj.start.position
            self.invalidate()
        if self.end != self.obj.end.position:
            self.end = self.obj.end.position
            self.invalidate()
        if self.directed != self.obj.directed:
            self.directed = self.obj.directed
            self.invalidate()
        info = self.obj.get_temp_value("t_info", "")
        if self.info != info:
            self.info = info
            self.invalidate()

    def render(self, canvas, transform):
        x1, y1 = transform(*self.start)
        x2, y2 = transform(*self.end)
        if self.selected:
            if self.highlighted:
                canvas.create_line(x1, y1, x2, y2, width=5,
                                   fill=get_color_from_palette("edge_highlight", self.palette, self.color),
                                   tag=self.tag())
            canvas.create_line(x1, y1, x2, y2, width=1.6,
                               fill=get_color_from_palette("edge_foreground", self.palette, self.color),
                               tag=self.tag())
            if self.directed:
                dx = self.end[0] - self.start[0]
                dy = self.end[1] - self.start[1]
                fi = atan2(dy, dx)
                r = self.arrow_size
                ang = pi - .3
                x3, y3 = self.end[0] + cos(fi) * -self.vertex_radius, self.end[1] + sin(fi) * -self.vertex_radius
                x4, y4 = transform(x3 + cos(fi + ang) * r, y3 + sin(fi + ang) * r)
                x5, y5 = transform(x3 + cos(fi - ang) * r, y3 + sin(fi - ang) * r)
                canvas.create_line(*transform(x3, y3), x4, y4, width=1.6,
                                   fill="black",  # get_color_from_palette("edge_foreground", self.palette, self.color),
                                   tag=self.tag())
                canvas.create_line(*transform(x3, y3), x5, y5, width=1.6,
                                   fill="black",  # get_color_from_palette("edge_foreground", self.palette, self.color),
                                   tag=self.tag())
        else:
            color = "#777" if self.highlighted else "#c8c8c8"
            canvas.create_line(x1, y1, x2, y2, width=1, fill=color, tag=self.tag())

        if self.info:
            canvas.create_text((x1 + x2) / 2, (y1 + y2) / 2, text=self.info, tag=self.tag())

    def event(self, name, position):
        task = self.task()
        if task is not None:
            bindings = {
                "left": "edge_select",
                "right": "edge_switch",
                "double": "edge_double"
            }
            if name in bindings:
                task.action(bindings[name], self.obj, self, position=position, task=task)

    def process_click(self, pos):
        dist = max(1e-6, segment_distance_sqr(*self.start, *self.end, *pos))
        if dist < self.check_distance ** 2:
            return 2, 1 / dist
        return None


class InterfaceUnitSheet(InterfaceUnit):
    def refresh(self):
        pass

    def render(self, canvas, transform):
        pass

    def process_click(self, pos):
        return 1, 0

    def event(self, name, position):
        task = self.task()
        if task is not None:
            bindings = {
                "left": "sheet_left",
                "right": "sheet_right",
                "drag": "sheet_drag",
                "double": "sheet_double"
            }
            if name in bindings:
                task.action(bindings[name], self.obj, self, position=position, task=task)


class InterfaceUnitGroup:
    def __init__(self, attached=None, source=(0, 0, 1, 1)):
        self.units = {}
        self.attached = attached  # object it is attached to

        self._sorted_units = []
        self._removed_units = []
        self._source = source
        self._highlighted = None
        self._dragged = None
        self._dragged_frames = 0
        self._task = None

        self.position_transform = Transform()
        self.screen_transform = Transform(self.position_transform)

    def assign_to(self, task):
        self._task = task

    def task(self):
        return self._task

    def transform(self, transform):
        self.position_transform.parent = transform

    def screen(self, dst):
        self.screen_transform.screen(self._source, dst)

    def render(self, canvas):
        dirty = False
        for unit in self._removed_units:
            unit.erase(canvas)
            if unit.uuid() in self.units:
                del self.units[unit.uuid()]
            if unit in self._sorted_units:
                self._sorted_units.remove(unit)
            dirty = True
        self._removed_units = []

        for unit in self._sorted_units:
            unit.refresh()
            if unit.dirty:
                dirty = True
        if dirty:
            self._sort()

        dirty = False
        for i in range(len(self._sorted_units)):
            unit = self._sorted_units[i]
            if unit.dirty:
                dirty = True
            else:
                unit.dirty = dirty
            unit.redraw_if_dirty(canvas, self.screen_transform)

    def find_attached(self, obj):
        for unit in self._sorted_units:
            if unit.obj is obj:
                return unit

    def remove(self, unit):
        self._removed_units.append(unit)

    def invalidate(self):
        for unit in self.units.values():
            unit.invalidate()

    def add(self, unit):
        unit.group = self
        self.units[unit.uuid()] = unit
        self._sorted_units.append(unit)
        unit.invalidate()

    def _sort(self):
        self._sorted_units.sort(key=lambda x: x.layer * 1024 + x.order)

    def erase(self, canvas):
        for unit in self.units.values():
            unit.erase(canvas)

    def clear(self):
        for unit in self.units.values():
            unit.erase()
            unit.group = None
        self.units = {}
        self._sorted_units = []

    def find(self, uuid):
        if uuid in self.units:
            return self.units[uuid]
        return None

    def process_action(self, action, position):
        position = self.screen_transform.transform_back(position)

        best_unit = None
        best_priority = (-1, -1)
        for unit in self.units.values():
            priority = unit.process_click(position)
            if priority and (priority[0] > best_priority[0] or
                             (priority[0] == best_priority[0] and priority[1] > best_priority[1])):
                best_unit = unit
                best_priority = priority
        if action == "highlight":
            if self._highlighted is not best_unit:
                if self._highlighted is not None:
                    self._highlighted.action("un-highlight", position)
                self._highlighted = best_unit
                if self._highlighted is not None:
                    self._highlighted.action("highlight", position)
        else:
            if action == "drag":
                if self._dragged is None:
                    self._dragged = best_unit
                best_unit = self._dragged
                self._dragged_frames += 1
            else:
                if action == "left" and self._dragged_frames > 3:
                    best_unit = None  # prevent calling left after drag
                self._dragged = None
                self._dragged_frames = 0
            if best_unit is not None:
                best_unit.action(action, position)
                return best_unit.uuid()
        return None


class InterfaceGroupContainer:
    def __init__(self, canvas=None):
        self.groups = []
        self.canvas = canvas

    def __iter__(self):
        return self.groups.__iter__()

    def __len__(self):
        return len(self.groups)

    def search_for(self, attached):
        found = []
        for group in self.groups:
            if group.attached is attached:
                found.append(group)
        return found

    def __getitem__(self, item):
        found = self.search_for(item)
        return found[0] if len(found) > 0 else None

    def __contains__(self, item):
        return len(self.search_for(item)) > 0

    def process_action(self, action, position):
        uuids = []
        for group in self.groups:
            uuid = group.process_action(action, position)
            if uuid is not None:
                uuids.append(uuid)
        return uuids

    def render(self):
        for group in self.groups:
            group.render(self.canvas)

    def add(self, group):
        self.groups.append(group)
        group.invalidate()

    def remove(self, group):
        if group in self.groups:
            self.groups.remove(group)
            group.erase(self.canvas)



