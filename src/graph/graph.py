import uuid
import random


class TemporaryEditableObject:
    def __init__(self):
        self._temp_values = {}

    def get_temp_value(self, key, default=None):
        if key in self._temp_values:
            return self._temp_values[key]
        else:
            return default

    def __getattr__(self, key):
        if len(key) >= 2 and key[:2] == "t_":
            return self._temp_values[key]
        else:
            return object.__getattribute__(self, key)

    def __setattr__(self, key, value):
        if len(key) >= 2 and key[:2] == "t_":
            self._temp_values[key] = value
        else:
            return object.__setattr__(self, key, value)

    def reset_properties(self, **kwargs):
        self._temp_values = kwargs


class Vertex(TemporaryEditableObject):
    def __init__(self, graph, pos=None):
        TemporaryEditableObject.__init__(self)
        self.graph = graph
        self.index = -1
        self.uuid = str(uuid.uuid4())  # used for render, instead of index is constant

        self.edges = []       # connected edges
        self.adjacent = []    # connected vertices
        self.active = True    # is vertex is active and should be added to graph
        self.locked = False   # locked vertices cant be modified by user
        self.color = 0        # color index of the vertex

        self.position = pos   # rendered position, set, as graph render is complete
        self.fixed = False    # if vertex is fixed, its relative position will be preserved

        graph.vertices.append(self)
        self.refresh()

    def copy_from(self, another):
        self.active = another.active
        self.locked = another.locked
        self.color = another.color
        self.position = another.position
        self.fixed = another.fixed
        self._temp_values = dict(another._temp_values)

    def refresh(self):
        if self.graph is not None:
            self.index = self.graph.get_vertex_index(self)
        else:
            self.index = -1

    def valid(self):
        return self.index != -1 and self.graph is not None

    # finds edge to this vertex
    def edge(self, vertex):
        for edge in self.edges:
            if vertex in edge:
                return edge

    # check if vertex is connected
    def connected(self, vertex):
        return vertex in self.adjacent

    # fixes if connection to vertex is missing, but appears in vertex
    def _fix_missing_connection(self, vertex):
        if self in vertex.adjacent:
            if vertex.graph is not self.graph:
                raise TypeError("trying to connect vertex to different graph")
            edge = vertex.edge(self)
            assert edge is not None
            self.adjacent.append(vertex)
            self.edges.append(edge)

    # connects to another vertex in the same graph
    def connect(self, vertex):
        if not self.connected(vertex):
            self._fix_missing_connection(vertex)
            if vertex not in self.adjacent:
                if vertex.graph is not self.graph:
                    raise TypeError("trying to connect vertex to different graph")
                edge = Edge(self, vertex)
                self.adjacent.append(vertex)
                vertex.adjacent.append(self)
                self.edges.append(edge)
                vertex.edges.append(edge)
                return edge
            else:
                self._fix_missing_connection(vertex)
        return self.edge(vertex)

    def disconnect(self, vertex):
        if vertex in self.adjacent:
            edge = self.edge(vertex)
            if edge is not None:
                self.edges.remove(edge)
            self.adjacent.remove(vertex)
        if self in vertex.adjacent:
            edge = vertex.edge(self)
            if edge is not None:
                vertex.edges.remove(edge)
            vertex.adjacent.remove(self)

    def disconnect_all(self):
        while len(self.adjacent) > 0:
            self.disconnect(self.adjacent[0])

    def serialize(self):
        self.refresh()
        data = dict(self.__dict__)
        for name in ("uuid", "graph", "edges", "adjacent"):
            if name in data:
                del data[name]
        return data

    def deserialize(self, data):
        for key, value in data.items():
            object.__setattr__(self, key, value)


class Edge(TemporaryEditableObject):
    def __init__(self, start, end):
        TemporaryEditableObject.__init__(self)

        assert start.graph is end.graph
        assert start is not end
        self.graph = start.graph
        self.start = start
        self.end = end
        self.directed = False
        self.uuid = str(uuid.uuid4())

        self.active = True    # is edge is active and should be added to graph
        self.locked = False   # locked edges cant be modified by user
        self.color = 0        # color index of the edge

    def copy_from(self, another):
        self.active = another.active
        self.locked = another.locked
        self.color = another.color
        self.directed = another.directed
        self._temp_values = dict(another._temp_values)

    # check if edge is between same vertices
    def same(self, edge):
        return (edge.start is self.start and edge.end is self.end) or (edge.end is self.start and edge.start is self.end)

    # changes direction to opposite
    def redirect(self):
        self.start, self.end = self.end, self.start

    def opposite(self, vertex):
        if not (vertex is self.start or vertex is self.end):
            raise TypeError("vertex is not in the edge")
        return self.end if vertex is self.start else self.start

    def start_at(self, v):
        assert v in self
        if v is not self.start:
            self.redirect()
        self.directed = True

    def end_at(self, v):
        assert v in self
        if v is not self.end:
            self.redirect()
        self.directed = True

    # returns aggregated weight of edge
    def weight(self):
        return 1

    def __contains__(self, item):
        return item is self.start or item is self.end

    def serialize(self):
        data = dict(self.__dict__)
        for name in ("uuid", "graph", "start", "end"):
            if name in data:
                del data[name]
        data["_start"] = self.start.index
        data["_end"] = self.end.index
        return data

    def deserialize(self, data):
        for key, value in data.items():
            object.__setattr__(self, key, value)


class GraphComponent:
    def __init__(self, graph, vertices):
        self.graph = graph
        self.vertices = vertices
        self.uuid = str(uuid.uuid4())

    def __contains__(self, item):
        return item in self.vertices

    def __iter__(self):
        return self.vertices.__iter__()

    def __len__(self):
        return len(self.vertices)

    def __getitem__(self, item):
        return self.vertices[item]

    def get_edges(self):
        edges = []
        for edge in self.graph.get_edges():
            if edge.start in self.vertices and edge.end in self.vertices:
                edges.append(edge)
        return edges

    # convert component to a independent separate graph
    def to_graph(self):
        edges = self.get_edges()
        for vertex in self.graph:
            vertex.t_com = vertex in self
        for edge in self.graph.get_edges():
            edge.t_com = edge in edges
        return self.graph.from_condition(lambda v: v.t_com, lambda e: e.t_com)


# List-like wrap of vertex sequence with edges data between them
class Path:
    def __init__(self, graph, vertices):
        self.graph = graph
        self.vertices = vertices
        self.uuid = str(uuid.uuid4())
        self.edges = [vertices[i].edge(vertices[i + 1]) for i in range(len(vertices) - 1)]
        self.connected = True
        for edge in self.edges:
            if edge is None:
                self.connected = False
                break

    def __contains__(self, item):
        if isinstance(item, Edge):
            return item in self.edges
        elif isinstance(item, Vertex):
            return item in self.vertices
        else:
            return False

    def __len__(self):
        return len(self.vertices)

    def __getitem__(self, item):
        if isinstance(item, slice):
            return Path(self.graph, self.vertices[item])
        else:
            return self.vertices[item]

    def __iter__(self):
        return self.vertices.__iter__()

    def index(self, v):
        return self.vertices.index(v)

    def edge(self, v1, v2):
        assert v1 in self and v2 in self
        i1 = self.vertices.index(v1)
        i2 = self.vertices.index(v2)
        if abs(i2 - i1) == 1:
            return v1.edge(v2)
        else:
            raise IndexError()

    def append(self, vertex):
        assert vertex.graph is self.graph
        if len(self.vertices) > 0:
            self.edges.append(self.vertices[-1].edge(vertex))
        self.vertices.append(vertex)

    def refresh(self):
        self.edges = [self.vertices[i].edge(self.vertices[i + 1]) for i in range(len(self.vertices) - 1)]

    def insert(self, index, vertex):
        assert vertex.graph is self.graph
        self.vertices.insert(index, vertex)
        self.refresh()

    def pop(self, *args):
        vertex = self.vertices.pop(*args)
        self.refresh()
        return vertex

    def remove(self, vertex):
        assert vertex.graph is self.graph
        self.vertices.remove(vertex)
        self.refresh()

    # remove all vertices that matches given path
    def subtract_vertices(self, path):
        for v in path:
            if v in self:
                self.remove(v)

    # remove all edges, that matches given path
    def subtract_edges(self, path):
        remove = []
        for vertex in self:
            for edge in vertex.edges:
                if edge in self.edges and edge not in path.edges:
                    break
            else:
                remove.append(vertex)
        for vertex in remove:
            self.remove(vertex)

    def reverse(self):
        self.vertices.reverse()
        self.edges.reverse()

    def __delitem__(self, key):
        del self.vertices[key]
        self.refresh()

    # split path into array of connected ones, singles=True will add single-vertex paths
    def split(self, singles=True):
        current = []
        split = []
        for i in range(len(self.vertices) - 1):
            v1 = self.vertices[i]
            v2 = self.vertices[i + 1]
            if len(current) == 0:
                current.append(v1)
            if v1.edge(v2) is not None:
                current.append(v2)
            else:
                if len(current) > 1 or singles:
                    split.append(Path(self.graph, current))
                current = []
        return split


# constant cyclic path
class Cycle:
    def __init__(self, graph, vertices):
        self.graph = graph
        self.vertices = vertices
        self.edges = [vertices[i].edge(vertices[(i + 1) % len(vertices)]) for i in range(len(vertices))]

    def __contains__(self, item):
        if isinstance(item, Edge):
            return item in self.edges
        elif isinstance(item, Vertex):
            return item in self.vertices
        else:
            return False

    def __len__(self):
        return len(self.vertices)

    def __getitem__(self, item):
        if isinstance(item, slice):
            return Path(self.graph, self.vertices[item])
        else:
            return self.vertices[item]

    def __iter__(self):
        return self.vertices.__iter__()


class GraphTopology:
    def __init__(self, graph):
        self.graph = graph
        self.matrix = []
        self.rebuild()

    def rebuild(self):
        self.matrix = [[0] * len(self.graph) for i in range(len(self.graph))]
        for v1 in self.graph:
            for v2 in self.graph:
                edge = v1.edge(v2)
                if edge is not None:
                    if edge.directed:
                        self.matrix[edge.start.index][edge.end.index] = 1
                    else:
                        self.matrix[edge.start.index][edge.end.index] = 1
                        self.matrix[edge.end.index][edge.start.index] = 1

    def __eq__(self, other):
        return self.matrix == other.matrix


class Graph:
    def __init__(self):
        self.vertices = []

        self.uuid = str(uuid.uuid4())

    def for_vertices(self, action):
        for vertex in self.vertices:
            action(vertex)

    def for_edges(self, action):
        for edge in self.get_edges():
            action(edge)

    # return current graph topology object
    def topology(self):
        return GraphTopology(self)

    # copy graph, all it vertices and edges
    def copy(self, uuids=None):
        if uuids is None:
            uuids = {}
        clone = Graph()
        for vertex in self:
            vertex_ = clone.add_vertex()
            vertex_.copy_from(vertex)
            uuids[vertex_.uuid] = vertex.uuid
        for edge in self.get_edges():
            edge_ = clone[edge.start.index].connect(clone[edge.end.index])
            edge_.copy_from(edge)
            uuids[edge_.uuid] = edge.uuid
        return clone

    def merge(self, graph):
        elements = []
        graph = graph.copy()
        for vertex in graph.vertices:
            vertex.graph = self
            elements.append(vertex)
            for edge in vertex.edges:
                edge.graph = self
                elements.append(edge)
            self.vertices.append(vertex)
            vertex.refresh()
        self.refresh()
        return elements

    def from_condition(self, vertex_filter, edge_filter):
        clone = self.copy()
        for vertex in clone:
            for edge in list(vertex.edges):
                opposite = edge.opposite(vertex)
                if not edge_filter(edge) or not vertex_filter(opposite):
                    vertex.disconnect(opposite)
        for vertex in list(clone.vertices):
            if not vertex_filter(vertex):
                clone.remove_vertex(vertex)
        return clone

    # return graph, that consist only from active vertices and edges
    def from_active(self):
        return self.from_condition(lambda x: x.active, lambda x: x.active)

    # activate/deactivate all vertices
    def set_active(self, active):
        for vertex in self.vertices:
            if not vertex.locked:
                vertex.active = active
            for edge in vertex.edges:
                if not edge.locked:
                    edge.active = active

    # remove all vertices with 2 or less connections, preserve edges between them
    def eliminate_paths(self, singles=False):
        for vertex in self.vertices[:]:
            if len(vertex.adjacent) <= 2:
                if len(vertex.adjacent) == 2:
                    vertex.adjacent[0].connect(vertex.adjacent[1])
                    vertex.disconnect_all()
                    self.remove_vertex(vertex)
                elif singles:
                    vertex.disconnect_all()
                    self.remove_vertex(vertex)

    # returns set of vertex colors
    def get_vertex_colors(self):
        colors = set()
        for vertex in self:
            colors.add(vertex.color)
        return colors

    # returns set of edge colors
    def get_edge_colors(self):
        colors = set()
        for edge in self.get_edges():
            colors.add(edge.color)
        return colors

    def find_vertex_color(self, banned=()):
        color = 0
        colors = self.get_vertex_colors()
        while color in colors or color in banned:
            color += 1
        return color

    def find_edge_color(self, banned=()):
        color = 0
        colors = self.get_edge_colors()
        while color in colors or color in banned:
            color += 1
        return color

    # adds some random edges and vertices, return added elements
    def randomize(self, iterations=1, group_chance=.2, vertex_chance=.5, edges_factor=1):
        elements = []
        for iteration in range(iterations):
            if random.random() < group_chance:
                new_group = Graph()
                new_group.randomize(iterations=random.randint(4, 6), vertex_chance=.75, group_chance=0)
                elements += self.merge(new_group)
            elif random.random() < vertex_chance:
                indices = list(range(len(self.vertices)))
                edges = random.randint(0, int(len(self.vertices) * random.random() * edges_factor)) + 1
                random.shuffle(indices)
                vertex = self.add_vertex(connections=indices[:edges])
                elements.append(vertex)
                elements += list(vertex.edges)
            else:
                indices = list(range(len(self.vertices)))
                if len(indices) > 1:
                    random.shuffle(indices)
                    for i in range(len(indices)):
                        v1 = self.vertices[indices[i - 1]]
                        v2 = self.vertices[indices[i]]
                        if not v1.connected(v2):
                            elements.append(v1.connect(v2))
                            break
        return elements

    def add_random_vertices(self, count):
        self.randomize(iterations=int(count), group_chance=0, vertex_chance=1)
        if random.random() < count - int(count):
            self.randomize(iterations=1, group_chance=0, vertex_chance=1)

    def add_random_edges(self, count):
        self.randomize(iterations=int(count), group_chance=0, vertex_chance=0)
        if random.random() < count - int(count):
            self.randomize(iterations=1, group_chance=0, vertex_chance=0)

    def add_random_components(self, count):
        self.randomize(iterations=int(count), group_chance=1, vertex_chance=0)
        if random.random() < count - int(count):
            self.randomize(iterations=1, group_chance=1, vertex_chance=0)

    # reset positions for all unfixed vertex, set force to True, to reset them all
    def reset_vertex_positions(self, force=False):
        for vertex in self.vertices:
            if not vertex.fixed or force:
                vertex.position = None

    def set_all_fixed(self, fixed):
        for vertex in self.vertices:
            vertex.fixed = fixed

    # return if all vertex positions are defined
    def nothing_to_simulate(self):
        for vertex in self.vertices:
            if vertex.position is None:
                return False
        return True

    def get_vertex_index(self, vertex):
        if vertex in self.vertices:
            return self.vertices.index(vertex)
        else:
            return -1

    def get_edges(self):
        edges = set()
        for vertex in self.vertices:
            for edge in vertex.edges:
                edges.add(edge)
        return edges

    def get_components(self):
        def recursive_get(vertex, component=None):
            if component is None:
                component = []
            else:
                if vertex in component:
                    return
            component.append(vertex)
            for adjacent in vertex.adjacent:
                recursive_get(adjacent, component=component)
            return component
        components = []
        for vertex in self.vertices:
            for component in components:
                if vertex in component:
                    break
            else:
                components.append(GraphComponent(self, recursive_get(vertex)))
        return components

    def connected(self):
        return len(self.get_components()) == 1

    # depth first search algorithm, returns count of explored vertices
    def dfs(self, start,
            vertex=lambda v, depth=0, parent=None, **kwargs: None,   # called if passed vertex
            edge=lambda e, depth=0, **kwargs: None,                  # called if passed edge
            path=lambda path, v, **kwargs: None,                     # called if passed a cyclic path
            condition=lambda v, **kwargs: True,                      # check if vertex can be passed
            directed=False,                  # directed edges can be passed only in one side
            undirected=True,                 # undirected edges can be passed
            _explored=None,
            _path=None,
            _last=None,
            **kwargs):  # kwargs are passed to vertex, path and edge callbacks
        assert start.graph is self
        if _path is None:
            _path = []
        if _explored is None:
            _explored = []

        depth = len(_path)
        index = len(_explored)
        if start in _explored:
            path(_path[:], start, parent=_last, index=index, depth=depth, **kwargs)
            return 0
        count = 1
        _explored.append(start)
        _path.append(start)
        vertex(start, parent=_last, index=index, depth=depth, path=_path[:], **kwargs)
        no_further_path = True
        for edge_ in start.edges:
            if edge_ in _explored:
                continue
            adjacent = edge_.opposite(start)
            if adjacent is not _last and (undirected or edge_.directed) and (not directed or not edge_.directed
                                                                             or edge_.end is adjacent):
                if condition(adjacent, parent=start, **kwargs):
                    no_further_path = False
                    edge(edge_, index=index, depth=depth, cycle=adjacent in _explored, **kwargs)
                    _explored.append(edge_)
                    count += self.dfs(adjacent, vertex=vertex, edge=edge, path=path, condition=condition,
                                      _explored=_explored, _path=_path, _last=start,
                                      directed=directed, undirected=undirected, **kwargs)
        if no_further_path:
            path(_path[:], None, parent=_last, index=index, depth=depth, **kwargs)
        _path.pop()
        return count

    # breadth first search algorithm, returns count of explored vertices
    def bfs(self, start,
            vertex=lambda v, depth=0, parent=None, **kwargs: None,   # called if passed vertex
            edge=lambda e, depth=0, parent=None, **kwargs: None,     # called if passed edge
            directed=False,                  # directed edges can be passed only in one side
            undirected=True,                 # undirected edges can be passed
            **kwargs):  # kwargs are passed to vertex, path and edge callbacks
        assert start.graph is self
        _path = []
        _explored = []

        def get_adjacent(v, _last):
            adjacent_ = []
            for edge_ in v.edges:
                adjacent = edge_.opposite(v)
                if adjacent is not _last and (undirected or edge_.directed) and (not directed or not edge_.directed
                                                                             or edge_.end is adjacent):
                    adjacent_.append(edge_)
            return adjacent_

        index_ = 0
        queue = [(start, None, [start], 0, index_)]
        while len(queue) > 0:
            current, last, path, depth, index = queue.pop(0)
            if last is not None:
                edge_ = last.edge(current)
                if edge_ is not None:
                    edge(last.edge(current), parent=last, depth=depth, index=index, path=path)
            vertex(current, depth=depth, parent=last, index=index, path=path, **kwargs)
            for edge_ in get_adjacent(current, last):
                vertex_ = edge_.opposite(current)
                if vertex_ not in _explored:
                    queue.append((vertex_, current, path + [vertex_], depth + 1, index_))
                    _explored.append(vertex_)
                    index_ += 1
        return index_

    def __len__(self):
        return len(self.vertices)

    def __getitem__(self, item):
        return self.vertices[item]

    def __iter__(self):
        return self.vertices.__iter__()

    def refresh(self):
        for vertex in self.vertices:
            vertex.refresh()

    def connect_vertices(self, v1, v2):
        if isinstance(v1, int):
            v1 = self.vertices[v1]
        if isinstance(v2, int):
            v2 = self.vertices[v2]
        assert v1.graph is v2.graph
        return v1.connect(v2)

    def add_vertex(self, connections=None):
        vertex = Vertex(self)
        if connections is not None:
            for connection in connections:
                if isinstance(connection, int):
                    connection = self[connection]
                vertex.connect(connection)
        return vertex

    def remove_vertex(self, vertex):
        if isinstance(vertex, int):
            vertex = self[vertex]
        if vertex in self.vertices:
            vertex.disconnect_all()
            self.vertices.remove(vertex)
        self.refresh()

    def remove_edge(self, edge):
        assert edge.graph is self
        edge.start.disconnect(edge.end)

    def remove_elements(self, *elements):
        for element in elements:
            if isinstance(element, Vertex):
                self.remove_vertex(element)
            elif isinstance(element, Edge):
                self.remove_edge(element)
            else:
                raise TypeError()

    def serialize(self):
        vertices = []
        edges = []
        for vertex in self.vertices:
            vertices.append(vertex.serialize())
        for edge in self.get_edges():
            edges.append(edge.serialize())
        return {
            "vertices": vertices,
            "edges": edges
        }

    def deserialize(self, data):
        vertices = data["vertices"]
        edges = data["edges"]
        if len(self.vertices) != 0:
            raise RuntimeError("deserializing into non-empty graph")

        self.vertices = []
        for vertex_data in vertices:
            vertex = Vertex(self)
            vertex.deserialize(vertex_data)
        for edge_data in edges:
            self.connect_vertices(edge_data["_start"], edge_data["_end"]).deserialize(edge_data)


if __name__ == "__main__":
    pass
